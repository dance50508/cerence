package com.mobiledrivetech.voiceassistant;

import static cerence.ark.assistant.library.Utils.getContext;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.car.Car;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.graphics.Color;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.blankj.utilcode.util.DeviceUtils;
import com.mobiledrivetech.voiceassistant.controller.VoiceAssistantController;
import com.mobiledrivetech.voiceassistant.eventbus.FinalOperation;
import com.mobiledrivetech.voiceassistant.eventbus.MessageInfo;
import com.mobiledrivetech.voiceassistant.hmi.CustomHmiListener;
import com.mobiledrivetech.voiceassistant.hmi.picklist.PickListManager;
import com.mobiledrivetech.voiceassistant.utils.DataUtils;
import com.mobiledrivetech.voiceassistant.voiceui.VoiceUIManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

import cerence.ark.assistant.api.ArkAssistant;
import cerence.ark.assistant.api.context.LatLng;
import cerence.ark.assistant.api.context.LocationContext;
import cerence.ark.assistant.api.context.LocationSource;
import cerence.ark.assistant.api.dynamic.ContentType;
import cerence.ark.assistant.api.notification.DialogEvent;
import cerence.ark.assistant.api.notification.DialogStateChanged;
import cerence.ark.assistant.framework.data.AssistantConfiguration;
import cerence.ark.assistant.framework.data.Config;
import cerence.ark.assistant.framework.data.InteractionMode;
import cerence.ark.assistant.framework.data.Language;
import cerence.ark.assistant.library.ICallBack;
import skin.support.SkinCompatManager;
import skin.support.app.SkinAppCompatViewInflater;
import skin.support.app.SkinCardViewInflater;
import skin.support.constraint.app.SkinConstraintViewInflater;
import skin.support.design.app.SkinMaterialViewInflater;

public class VoiceAssistantService extends Service {
    String TAG = this.getClass().getSimpleName();

    private static final int NOTIFICATION_VOICE_ASSISTANT_SERVICE = 1;

    private android.location.Location gpsLocation = new android.location.Location(LocationManager.GPS_PROVIDER),
            netwoekLocation = new android.location.Location(LocationManager.NETWORK_PROVIDER);
    private LocationManager mLocationManager;

    //Integrate theme observer
    private SettingsObserver mSettingsObserver;
    private ContentResolver mContentResolver;

    private static Car mCar;

    public VoiceAssistantService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();

        runAsForeground();
        EventBus.getDefault().register(this);

        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        //Integrate theme observer
        mContentResolver = this.getContentResolver();
        mSettingsObserver = new SettingsObserver(new Handler());
        mContentResolver.registerContentObserver(Settings.Global.getUriFor(
                "current_theme"),
                false, mSettingsObserver);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand: intent = " + intent);

        initVoiceAssistant();
        initSkinCompatManager();
        connectCar();

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        destroyVoiceAssistant();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void runAsForeground() {
        String NOTIFICATION_CHANNEL_ID = "com.evenwell.clusterinfo";
        String channelName = "My Background Service";
        NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.createNotificationChannel(chan);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        Notification notification = notificationBuilder.setOngoing(true)
                .setContentTitle("App is running in background")
                .setPriority(NotificationManager.IMPORTANCE_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build();
        startForeground(NOTIFICATION_VOICE_ASSISTANT_SERVICE, notification);
    }

    private void connectCar() {
        mCar = Car.createCar(getApplicationContext());
    }

    public static Car getConnectedCar(){
        return mCar;
    }




    protected void initVoiceAssistant() {
        Log.d(TAG, "Initializing Voice Assistant, Please wait...");
//        runOnUiThread(() -> {
//            sdkState.setText("Initializing Voice Assistant, Please wait...");
//            sdkState.setTextColor(getColor(R.color.black));
//        });
        Runnable runnable = () -> {
            String dataPath = getApplicationContext().getExternalFilesDir("data").getAbsolutePath();
            String writePath = getApplicationContext().getExternalFilesDir("file").getAbsolutePath();

            Log.d(TAG, "dataPath = "+dataPath);
            Log.d(TAG, "writePath = "+writePath);

            DataUtils.copyData(getApplicationContext(), dataPath);
            Config config = new Config.Builder().setAsrLanguage(Language.ENG_USA)
                    .userId(DeviceUtils.getUniqueDeviceId())
                    .dataPath(dataPath)
                    .writablePath(writePath)
                    .isSaveLog(true)
                    .isSaveInputAudio(false)
                    .isSaveOutputAudio(false)
                    .build();
            ArkAssistant.get()
                    .initializeVoiceAssistant(getApplicationContext(), config, new CustomHmiListener(), new VoiceAssistantController(), new ICallBack() {
                        @Override
                        public void onSuccess() {
                            Log.d(TAG, "init void assistant success");
//                            runOnUiThread(() -> {
//                                sdkState.setText("Voice assistant initialized");
//                                sdkState.setTextColor(getColor(R.color.black));
//                            });
                            notifyContentChanged();
                            startVoiceAssistant();
                        }

                        @Override
                        public void onFailure(String errorMessage) {
//                            hideProgress();
//                            runOnUiThread(() -> {
//                                sdkState.setText("Voice assistant not initialized");
//                                sdkState.setTextColor(getColor(android.R.color.holo_red_light));
//                            });
                            Log.e(TAG, String.format("init fail, error message = %s", errorMessage));
//                            Toast.makeText(BaseImplActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                        }
                    });


        };
        new Thread(runnable).start();



    }

    private void initSkinCompatManager(){
        Log.d(TAG, "initSkinCompatManager");
        SkinCompatManager.withoutActivity(getApplication())
                .addInflater(new SkinAppCompatViewInflater())           // 基础控件换肤初始化
                .addInflater(new SkinMaterialViewInflater())            // material design 控件换肤初始化[可选]
                .addInflater(new SkinConstraintViewInflater())          // ConstraintLayout 控件换肤初始化[可选]
                .addInflater(new SkinCardViewInflater())                // CardView v7 控件换肤初始化[可选]
                .setSkinStatusBarColorEnable(false)                     // 关闭状态栏换肤，默认打开[可选]
                .setSkinWindowBackgroundEnable(false)                   // 关闭windowBackground换肤，默认打开[可选]
                .loadSkin(CarVoiceAssistantApplication.ThemeType, new SkinCompatManager.SkinLoaderListener() {
                    @Override
                    public void onStart() {
                        Log.d(TAG, "initSkinCompatManager: onStart");
                    }

                    @Override
                    public void onSuccess() {
                        Log.d(TAG, "initSkinCompatManager: onSuccess");

                    }

                    @Override
                    public void onFailed(String s) {
                        Log.d(TAG, "initSkinCompatManager: onFailed => "+s);
                    }
                }, SkinCompatManager.SKIN_LOADER_STRATEGY_INTERNAL_STORAGE);
    }

    protected void notifyContentChanged() {
        boolean hasGps = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean hasNetwork = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (hasGps) {
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0F, new GpsLocationListener());
        }
        if (hasNetwork) {
            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 0F, new NetworkLocationListener());
        }

        ArkAssistant.get().notifyContentChanged(ContentType.CONTACT);
        ArkAssistant.get().notifyContentChanged(ContentType.MUSIC);
        ArkAssistant.get().notifyContentChanged(ContentType.APP);
    }

    protected void destroyVoiceAssistant() {
        ArkAssistant.get().destroyVoiceAssistant(null);
//        runOnUiThread(() -> {
//            sdkState.setText("Voice assistant not initialized");
//            sdkState.setTextColor(getColor(android.R.color.holo_red_light));
//        });
    }

    protected void startVoiceAssistant() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_name", "cerence");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ArrayList<String> wuwList = new ArrayList<>();
        wuwList.add("hi airflow");
        wuwList.add("hey airflow");
        AssistantConfiguration.Builder builder = new AssistantConfiguration.Builder();
        AssistantConfiguration configuration = builder.enableBargeIn(false).interactionMode(InteractionMode.WUW).setWakeupWords(wuwList).build();

        ArkAssistant.get().startVoiceAssistant(configuration, new ICallBack() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "start Assistant success");
                //VoiceBarManager.get().init(getApplicationContext());
                VoiceUIManager.getInstance().init(getApplicationContext());
                PickListManager.get().setControlPicklist(VoiceUIManager.getInstance());

//                hideProgress();
//                runOnUiThread(() -> {
//                    Toast.makeText(BaseImplActivity.this, "Ready", Toast.LENGTH_SHORT).show();
//                    sdkState.setText("Voice assistant running");
//                    sdkState.setTextColor(getColor(android.R.color.holo_green_dark));
//                });
            }

            @Override
            public void onFailure(String errorMessage) {
                Log.e(TAG, "start Assistant fail :" + errorMessage);
//                hideProgress();
            }
        });
    }

    protected void stopVoiceAssistant() {
        ArkAssistant.get().stopVoiceAssistant(new ICallBack() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "stop Assistant success");
//                runOnUiThread(() -> {
//                    sdkState.setText("Voice assistant stopped");
//                    sdkState.setTextColor(getColor(android.R.color.black));
//                });
            }

            @Override
            public void onFailure(String errorMessage) {
                Log.d(TAG, "stop Assistant fail :" + errorMessage);
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(MessageInfo messageEvent) {
        Log.d(TAG, "onMessage: messageEvent = "+messageEvent.getJson());
//        finalAction.setText(messageEvent.getJson());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(String hints) {
        if (!TextUtils.isEmpty(hints)) {
            Log.d(TAG, "onMessage hints: "+hints);
//            finalAction.setText(hints);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(DialogEvent dialogEvent) {
        MessageInfo info = new MessageInfo();
        info.setType(dialogEvent.getName());
        info.setJson(dialogEvent.getEvent().name());
        Log.d(TAG, "onMessage: dialogEvent = "+dialogEvent.getEvent().name());
//        notificationAdapter.addData(info);
//        notificationRecycleView.scrollToPosition(notificationAdapter.getItemCount() - 1);
        if (dialogEvent.getEvent() == DialogEvent.Event.CONVERSATION_END) {
            PickListManager.get().hidePickList();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(DialogStateChanged dialogStateChanged) {
        MessageInfo info = new MessageInfo();
        info.setType(dialogStateChanged.getName());
        info.setJson(dialogStateChanged.getState().name());
        Log.d(TAG, "onMessage: dialogStateChanged = "+dialogStateChanged.getState());
//        stateAdapter.addData(info);
//        stateRecycleView.scrollToPosition(stateAdapter.getItemCount() - 1);
        if (dialogStateChanged.getState() == DialogStateChanged.State.IDLE ||
                dialogStateChanged.getState() == DialogStateChanged.State.LISTENING_WUW) {
            PickListManager.get().hidePickList();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(FinalOperation operation) {
        if (operation.isToast()) {
            Toast.makeText(this, operation.getAction(), Toast.LENGTH_SHORT).show();
        }
        Log.d(TAG, "onMessage: operation = "+operation.getAction());

//        finalAction.setText(operation.getAction());
    }

    public class GpsLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(@NonNull android.location.Location location) {
            Date date = new Date(location.getTime());
            Log.d(TAG, "onGPSLocationChanged: " + date.toString() + "\tlatitude:" + location.getLatitude() + " longitude:" + location.getLongitude());
            gpsLocation = location;
            setLocationToCerence();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(@NonNull String provider) {

        }

        @Override
        public void onProviderDisabled(@NonNull String provider) {

        }
    }

    public class NetworkLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(@NonNull android.location.Location location) {
            Date date = new Date(location.getTime());
            Log.d(TAG, "onNetworkLocationChanged: " + date.toString() + "\tlatitude:" + location.getLatitude() + " longitude:" + location.getLongitude());
            netwoekLocation = location;
            setLocationToCerence();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(@NonNull String provider) {

        }

        @Override
        public void onProviderDisabled(@NonNull String provider) {
        }
    }


    private final class SettingsObserver extends ContentObserver {
        public SettingsObserver(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            String type = Settings.Global.getString(mContentResolver, "current_theme");
            Log.d(TAG, "onChange: current_theme type = "+type);
            if(!type.equals(CarVoiceAssistantApplication.ThemeType)){
                CarVoiceAssistantApplication.ThemeType =type;
                setTheme();
            }
        }
    }
    private void setTheme(){
        Log.d(TAG, "setTheme: type ="+CarVoiceAssistantApplication.ThemeType);
        if(!CarVoiceAssistantApplication.ThemeType.equals(CarVoiceAssistantApplication.THEME_TYPE_DEF)) {
            SkinCompatManager.getInstance().loadSkin(CarVoiceAssistantApplication.ThemeType, null,
                    SkinCompatManager.SKIN_LOADER_STRATEGY_INTERNAL_STORAGE);
        } else {
            SkinCompatManager.getInstance().restoreDefaultTheme();

        }
    }

    private void setLocationToCerence() {
        android.location.Location lastKnownLocationByGps = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (lastKnownLocationByGps != null) gpsLocation = lastKnownLocationByGps;
        android.location.Location lastKnownLocationByNetwork = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (lastKnownLocationByNetwork != null) netwoekLocation = lastKnownLocationByNetwork;

        android.location.Location currentLocation;
        double latitude = 0, longitude = 0;
        if (gpsLocation != null && netwoekLocation != null) {
            if (gpsLocation.getAccuracy() > netwoekLocation.getAccuracy()) {
                currentLocation = gpsLocation;
            } else {
                currentLocation = netwoekLocation;
            }
            latitude = currentLocation.getLatitude();
            longitude = currentLocation.getLongitude();
        }
        Log.d(TAG, "setLocationToCerence: latitude:" + latitude + " longitude:" + longitude);

        LatLng latLng = new LatLng(latitude, longitude);
        LocationContext locationContext = new LocationContext.Builder().setCurrentLocation(latLng)
                .setCurrentLocationSource(LocationSource.GPS)
                .build();
        ArkAssistant.get().updateAssistantContext(locationContext);
    }

}