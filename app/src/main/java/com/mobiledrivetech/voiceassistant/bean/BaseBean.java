package com.mobiledrivetech.voiceassistant.bean;


public class BaseBean {
    private int type;
    private String utterance;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getUtterance() {
        return utterance;
    }

    public void setUtterance(String utterance) {
        this.utterance = utterance;
    }
}
