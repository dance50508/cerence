package com.mobiledrivetech.voiceassistant;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class VoiceassistantBootCompleteReceiver extends BroadcastReceiver {
    String TAG= this.getClass().getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.w(TAG, "onReceive: " + intent.getAction());

        switch (action) {
            case Intent.ACTION_BOOT_COMPLETED:
                intent.setClass(context, VoiceAssistantService.class);
                context.startForegroundService(intent);
                break;
            default:
                break;
        }
    }
}
