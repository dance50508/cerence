package com.mobiledrivetech.voiceassistant.music;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.text.TextUtils;
import android.util.Log;

import com.mobiledrivetech.voiceassistant.constants.VoiceConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import cerence.ark.assistant.controller.enumeration.MediaStateEnum;

public class MusicController {
    private static final String TAG = MusicController.class.getSimpleName();

    private Context mContext;
    @SuppressLint("StaticFieldLeak")
    private static MusicController mInstance;
    private int mPos = -1;
    private String mKeyWord;

    private String mediaState = MediaStateEnum.UNKNOWN;

//    private List<CarOSControlInfoData> localResult = new ArrayList<>();
//    private List<MusicData> onlineResult = new ArrayList<>();
//    private List<SingerListResponse.ResponseBean.DocsBean> tobResult = new ArrayList<>();
//    private HashMap<String, String> mStyleType = new HashMap<String, String>() {
//        {
//            put("成名曲", "一人一首成名曲");
//            put("经典", "经典好歌");
//            put("经典翻唱", "翻唱");
//            put("发烧", "发烧音乐");
//            put("奥斯卡", "奥斯卡金曲");
//            put("游戏", "游戏原声");
//            put("游戏，游戏原声", "游戏原声");
//            put("电视原声，电视剧", "电视原声");
//            put("电视剧", "电视原声");
//            put("国语", "华语");
//            put("车载", "开车");
//            put("spa", "SPA馆");
//            put("工作，学习", "工作学习");
//            put("工作", "工作学习");
//            put("学习", "工作学习");
//            put("咖啡", "咖啡时光");
//            put("毕业礼，毕业典礼", "毕业礼");
//            put("塞车时", "塞车");
//            put("堵车时", "塞车");
//            put("堵车", "塞车");
//            put("骑自行车", "骑车");
//            put("公交", "公共交通");
//            put("地铁", "公共交通");
//            put("巴士", "公共交通");
//            put("轻轨", "公共交通");
//            put("轮船", "公共交通");
//            put("高铁", "公共交通");
//            put("飞机", "公共交通");
//            put("灵魂", "灵魂乐");
//            put("电子流派", "电子");
//            put("热歌", "流行");
//            put("想念", "思念");
//            put("下雨天", "雨");
//            put("晴天", "晴");
//            put("下雪", "雪");
//            put("分手", "失恋");
//            put("夜晚", "深夜");
//            put("八十年代", "80年代");
//            put("七十年代", "70年代");
//            put("六十年代", "60年代");
//            put("五十年代", "50年代");
//            put("一零年代", "10年代");
//            put("零零年代", "00年代");
//            put("零零后", "00后");
//            put("九零后", "90后");
//            put("八零后", "80后");
//            put("七零后", "70后");
//        }
//    };

//    private HashMap<String, String> mFMStyleType = new HashMap<String, String>() {
//        {
//            put("抖音", "抖音:音乐");
//        }
//    };

    private MusicController(Context context) {
        mContext = context;
        MusicClient.getInstance(context);
    }

    public static MusicController getInstance(Context context) {
        if (mInstance == null) {
            synchronized (MusicController.class) {
                if (mInstance == null) {
                    mInstance = new MusicController(context.getApplicationContext());
                }
            }
        }
        return mInstance;
    }

    private int getIndex(boolean random, int size) {
        Log.i(TAG, "size = " + size);
        return random ? new Random().nextInt(size) : 0;
    }

    /* 处理数据并选择搜索策略
     * 本地搜索优先搜索：
     *      1、歌曲名存在
     *      2、歌手名存在
     * 在线搜索：
     *      1、本地搜索无果
     *      2、专辑搜索
     *      */

    public void parseMusicData(String data) {
        try {
            Log.i(TAG, " -------- parseMusicData ");
            resetData();
            JSONObject model = new JSONObject(data);
            String albumName = model.optString("album");
            String musicName = model.optString("title");
            String singerName = model.optString("artist");

            String target = null;

//            JSONArray jsonArray = model.optJSONArray("artist");
//            for (int i = 0; i < jsonArray.length(); i++) {
//                String msg = jsonArray.getString(i);
//                singerNameList.add(msg);
//            }
//            if (singerNameList.size() > 0) {
//                singerName = singerNameList.get(0);
//            } else {
//                singerName = null;
//            }
//            JSONArray keywordArray = model.optJSONArray("keywords");
//            if (keywordArray != null && keywordArray.length() > 0) {
//                mKeyWord = keywordArray.optString(0);
//            }
            Log.i(TAG, " singer = " + singerName + " , song = " + musicName + " ,album = " + albumName + " keyword =" + mKeyWord);

            String result = reorganization(singerName, musicName, albumName);
            Log.i(TAG, " playWithoutSearch result >>" + result);
            playWithoutSearch(result);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void resetData() {
//        localResult.clear();
//        onlineResult.clear();
//        tobResult.clear();
        mKeyWord = null;
    }

//    private void startSearch(String singerName, String musicName, String albumName, String target) {
//        if (hasUsbDevices() && (!TextUtils.isEmpty(musicName) || (!TextUtils.isEmpty(singerName)
//                && !Constants.Music.MUSIC_TARGET_ALBUM.equals(target)))) {
//            LogTools.i(TAG, "search local_music");
//            localResult = new SearchLocal().searchLocalMusic(mContext, singerName, musicName); //按照歌手名|歌曲名查找(本地)
//        }
//        //本地无结果，在线搜索
//        if (localResult != null && localResult.size() <= 0) {
//            Log.i(TAG, "search online_music");
//            onlineResult = SearchOnline.getInstance(mContext).searchOnlineMusic(singerName, musicName, albumName, target);
//            if (onlineResult != null && onlineResult.size() <= 0) {
//                Log.i(TAG, "search tob_music");
//                tobResult = new SearchOnLineTob(mContext).searchOnlineMusicTOB(singerName, musicName, albumName, target);
//            }
//        }
//
//        if (localResult != null && localResult.size() > 0) {
//            mPos = getIndex((TextUtils.isEmpty(musicName) && (localResult.size() > 1)), localResult.size() - 1);
//        } else if ((tobResult != null && tobResult.size() > 0)) {
//            mPos = getIndex((TextUtils.isEmpty(musicName) && (tobResult.size() > 1)), tobResult.size() - 1);
//        } else if ((onlineResult != null && onlineResult.size() > 0)) {
//            mPos = getIndex((TextUtils.isEmpty(musicName) && (onlineResult.size() > 1)), onlineResult.size() - 1);
//        }
//    }

    private String reorganization(String singerName, String musicName, String albumName) {
        String result = "";
        if (!TextUtils.isEmpty(musicName)) {
            result += "song:" + musicName;
        }
        if (!TextUtils.isEmpty(singerName)) {
            if (!TextUtils.isEmpty(result))
                result += ", ";
            result += "singer:" + singerName;
        }
        if (!TextUtils.isEmpty(albumName)) {
            if (!TextUtils.isEmpty(result))
                result += ", ";
            result += "album:" + albumName;
        }
        if (!TextUtils.isEmpty(mKeyWord) && result.contains("song")) {
            result.replaceFirst(musicName, mKeyWord);
        } else if (!TextUtils.isEmpty(mKeyWord)) {
            if (!TextUtils.isEmpty(result))
                result += ", ";
            result += "song:" + mKeyWord;
        }
        if (TextUtils.isEmpty(result))
            result = "song:music_random";

        return result;
    }

    private void playWithoutSearch(String keyWord) {
        MusicClient.getInstance(mContext).playAfterTTS(keyWord, 16);
    }

//    public void play() {
//        if (localResult != null && localResult.size() > 0) {
//            MusicClient.getInstance(mContext).play(localResult, mPos, Constants.Music.PLAY_TYPE_USB);
//        } else if ((tobResult != null && tobResult.size() > 0)) {
//            MusicClient.getInstance(mContext).play(tobResult, mPos, Constants.Music.PLAY_TYPE_ONLINE);
//        } else if ((onlineResult != null && onlineResult.size() > 0)) {
//            MusicClient.getInstance(mContext).play(onlineResult, mPos, Constants.Music.PLAY_TYPE_ONLINE);
//        } else if (!TextUtils.isEmpty(mKeyWord)) {
//            Log.i(TAG, "send style = " + mKeyWord);
//            if (mFMStyleType.containsKey(mKeyWord)) {
//                Log.i(TAG, "send FM style = " + mKeyWord);
//                OnlineFMController.getInstance(mContext).doRealSearch(mFMStyleType.get(mKeyWord));
//                return;
//            }
//            if (mStyleType.containsKey(mKeyWord)) {
//                mKeyWord = mStyleType.get(mKeyWord);
//            }
//            Log.i(TAG, "send music style = " + mKeyWord);
//            MusicClient.getInstance(mContext).playAfterTTS(mKeyWord, 14);
//        } else {
//            TXZResourceManager.getInstance().speakTextOnRecordWin("找不到相关歌曲", false, null);
//        }
//    }

    public void release() {
        if (MusicClient.getInstance(mContext) != null) {
            MusicClient.getInstance(mContext).unbindFromPlayService(mContext);
        }
    }

    //发送广播给当前audio focus应用
    public void audioController(String action, int type) {
        String focusPackage = null;
//        if (type == 1) {
//            focusPackage = getAudioFocusPackageName();
//        } else if (type == 2) {
        focusPackage = VoiceConstants.Music.ONEMUSIC_PACKAGE;
//        }
        if (TextUtils.isEmpty(focusPackage)) return;
        Intent intent = new Intent(VoiceConstants.Music.ACTION_AUDIO);
        intent.putExtra(VoiceConstants.Music.AUDIO_STATUS, action);
        intent.putExtra(VoiceConstants.Music.AUDIO_PACKAGE, focusPackage);
        mContext.sendBroadcast(intent);
        Log.i(TAG, " focusPackage : " + focusPackage + "   action : " + action);
    }

    //发送广播给指定包名应用
//    public void audioController(String action, String pkgName) {
//        Intent intent = new Intent(Constants.Music.ACTION_AUDIO);
//        intent.putExtra(Constants.Music.AUDIO_STATUS, action);
//        intent.putExtra(Constants.Music.AUDIO_PACKAGE, pkgName);
//        mContext.sendBroadcast(intent);
//        LogTools.i(TAG, " pkgName : " + pkgName + "   action : " + action);
//    }

    //获取当前持有音频焦点持有者
//    public String getAudioFocus() {
//        String pkgName = null;
//        try {
//            Log.i(TAG, " do getAudioFocus ");
//            AudioManager am = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
//            Class<AudioManager> cls = AudioManager.class;//首先还是必须得到这个对象的Class。
//            Method getAudioFocusPackageName = cls.getDeclaredMethod("getCurrentAudioFocusPackageName");//得到执行的method
//            getAudioFocusPackageName.setAccessible(true);
//            pkgName = getAudioFocusPackageName.invoke(am).toString();
//            Log.i(TAG, " AudioFocusPackageName : " + pkgName);
//        } catch (Exception e) {
//            Log.i(TAG, " Exception : " + e.toString() + "   AudioFocusPackageName : " + pkgName);
//            e.getStackTrace();
//        }
//        return pkgName;
//    }

    //获取当前持有音频焦点栈是否存在 OneMusic/BlueTooth Music,包含则返回对应包名，否则返回""
    public String getAudioFocusPackageName() {
        List<String> pkgList;
        try {
            Log.i(TAG, " do getAudioFocusPackageName ");
            AudioManager am = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
            Class<AudioManager> cls = AudioManager.class;//首先还是必须得到这个对象的Class。
            Method getAudioFocusStackPackageName = cls.getDeclaredMethod("getAudioFocusStackPackageName");//得到执行的method
            getAudioFocusStackPackageName.setAccessible(true);
            pkgList = (List<String>) getAudioFocusStackPackageName.invoke(am);
            Log.i(TAG, " AudioFocusPackageName : " + Arrays.toString(pkgList.toArray()));
            if (pkgList.contains(VoiceConstants.Music.ONEMUSIC_PACKAGE))
                return VoiceConstants.Music.ONEMUSIC_PACKAGE;
            else if (pkgList.contains(VoiceConstants.Music.BLUETOOTH_PACKAGE))
                return VoiceConstants.Music.BLUETOOTH_PACKAGE;
        } catch (Exception e) {
            Log.i(TAG, " Exception : " + e.toString() + "   AudioFocusPackageName : " + null);
            e.getStackTrace();
        }
        return "";
    }

//    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
//    private boolean hasUsbDevices() {
//        UsbManager usbManager = (UsbManager) mContext.getSystemService(Context.USB_SERVICE);
//        if (usbManager == null) return false;
//        HashMap<String, UsbDevice> devices = usbManager.getDeviceList();
//        if (null != devices && devices.size() > 0) {
//            Log.i(TAG, "hasUsbDevices devices.size() = " + devices.size());
//            try {
//                for (Map.Entry<String, UsbDevice> entry : devices.entrySet()) {
//                    UsbDevice temp = entry.getValue();
//                    if (null != temp) {
//                        Log.i(TAG, "hasUsbDevices = " + temp.toString());
//                        int i = temp.getConfigurationCount();
//                        for (int ii = 0; ii < i; ii++) {
//                            UsbConfiguration usbConfiguration = temp.getConfiguration(ii);
//                            int j = usbConfiguration.getInterfaceCount();
//                            for (int jj = 0; jj < j; jj++) {
//                                UsbInterface usbInterface = usbConfiguration.getInterface(jj);
//                                if (usbInterface.getInterfaceClass() == UsbConstants.USB_CLASS_MASS_STORAGE) {
//                                    return true;
//                                }
//                            }
//                        }
//                    }
//                }
//                return false;
//            } catch (Exception e) {
//                Log.e(TAG, "hasUsbDevices Exception " + e.toString());
//                return false;
//            }
//        }
//        return false;
//    }

    public void setMediaState(String mediaState) {
        this.mediaState = mediaState;
    }

    public String getMediaState() {
        return mediaState;
    }
}
