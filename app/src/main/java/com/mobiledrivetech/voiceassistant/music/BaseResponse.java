package com.mobiledrivetech.voiceassistant.music;

import android.os.Parcel;
import android.os.Parcelable;


public class BaseResponse implements Parcelable{

    private int source;
    private int playCategoryID;//用于toB itemID
    private int playSpecialID;//用于toC itemID
    private ResponseHeaderBean responseHeader;

    public BaseResponse(){

    }

    public BaseResponse(int status, String prompt){
        this.responseHeader.status = status;
        this.responseHeader.errorinfo = prompt;
    }

    public int getSource() {
        return source;
    }

    public void setSource(int source) {
        this.source = source;
    }

    public int getPlayCategoryID() {
        return playCategoryID;
    }

    public void setPlayCategoryID(int categoryID) {
        this.playCategoryID = categoryID;
    }

    public int getPlaySpecialID() {
        return playSpecialID;
    }

    public void setPlaySpecialID(int specialID) {
        this.playSpecialID = specialID;
    }

    public ResponseHeaderBean getResponseHeader() {
        return responseHeader;
    }

    public void setResponseHeader(ResponseHeaderBean responseHeader) {
        this.responseHeader = responseHeader;
    }
    public static class ResponseHeaderBean {
        /**
         * errorinfo :
         * status : 00
         */

        private String errorinfo;
        private int status;

        public String getErrorinfo() {
            return errorinfo;
        }

        public void setErrorinfo(String errorinfo) {
            this.errorinfo = errorinfo;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }

    public static final Creator<BaseResponse> CREATOR = new Creator<BaseResponse>() {
        @Override
        public BaseResponse createFromParcel(Parcel source) {
            /*BaseResponse base = new BaseResponse();
            base.setSource(source.readInt());
            base.setPlayCategoryID(source.readInt());
            base.setPlaySpecialID(source.readInt());
            return base;*/
            return new BaseResponse();
        }

        @Override
        public BaseResponse[] newArray(int size) {
            return new BaseResponse[size];
        }
    };
 }
