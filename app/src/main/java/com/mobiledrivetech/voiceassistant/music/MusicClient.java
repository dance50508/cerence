package com.mobiledrivetech.voiceassistant.music;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;


import com.mobiledrivetech.onemusic.IMediaPlaybackService;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class MusicClient {
    private static final String TAG = "MusicClient";

    public static final String MUSIC_PACKAGE = "com.mobiledrivetech.onemusic";
    public static final String ONLINE_MUSIC_SERVICE_NAME = "com.mobiledrivetech.onemusic.service.MediaPlaybackService";

    private IMediaPlaybackService mService = null;
    private Context mContext;
    private static MusicClient mInstance;
    private int mMusicType;
    private ReentrantLock lock = new ReentrantLock();
    private Condition condition = lock.newCondition();
    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            lock.lock();
            try {
                mService = IMediaPlaybackService.Stub.asInterface(service);
                Log.i(TAG, " bind play service successfully");
                condition.signalAll();
            } finally {
                lock.unlock();
            }

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.i(TAG, "Fail to bind play service");
            mService = null;
            bindToPlayService(mContext);
        }
    };

    private MusicClient(Context context) {
        mContext = context.getApplicationContext();
        bindToPlayService(context);
    }

    public static MusicClient getInstance(Context context) {
        if (mInstance == null) {
            synchronized (MusicClient.class) {
                if (mInstance == null) {
                    mInstance = new MusicClient(context);
                }
            }
        }
        return mInstance;
    }

    public void play(List<? extends BaseResponse> list, int pos, int type) {
        if (mService != null) {
            Log.i(TAG, "play_list_size = " + list.size() + " , pos = " + pos + " , play_type = " + type);
            try {
                mService.openList(list, pos, type);
                mService.setCallSource("com.mobiledrivetech.voiceassistant");
                mService.play(type);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        } else {
            Log.i(TAG, " service is not available");
        }
    }

    public void playAfterTTS(String radioType, int playType) {
        if (mService == null) {
            bindToPlayService(mContext);
            doLock();
        }
        if (mService != null) {
            Log.i(TAG, "play radio type");
            try {
                mService.setCallSource("com.mobiledrivetech.voiceassistant");
                mService.setCallForRadioType(radioType);
                mService.play(playType); //按风格播放曲目
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        } else {
            Log.i(TAG, " service is not available");
        }
    }

    public void playWithoutTTS(String radioType, int playType) {
        if (mService == null) {
            bindToPlayService(mContext);
            doLock();
        }
        if (mService != null) {
            try {
                Log.i(TAG, "play radio type");
                mService.setCallSource("com.mobiledrivetech.voiceassistant");
                mService.setCallForRadioType(radioType);
                mService.play(playType); //按风格播放曲目
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Log.i(TAG, " service is not available");
        }
    }

    public void play(int type) {
        if (mService == null) {
            bindToPlayService(mContext);
            doLock();
        }
        if (mService != null) {
            try {
                if (type != 0) {
                    mMusicType = type;
                }
                Log.i(TAG, "camera music type : " + mMusicType);
                mService.setCallSource("com.mobiledrivetech.voiceassistant");
                mService.play(mMusicType);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        } else {
            Log.i(TAG, " service is not available");
        }
    }

    private void bindToPlayService(Context context) {
        if (context != null) {
            Log.i(TAG, " start bind service");
            Intent service = new Intent();
            service.setComponent(new ComponentName(MUSIC_PACKAGE, ONLINE_MUSIC_SERVICE_NAME));
            context.bindService(service, conn, Service.BIND_AUTO_CREATE);
        } else {
            Log.i(TAG, " context is not ready");
        }
    }

    public void unbindFromPlayService(Context context) {
        try {
            Log.i(TAG, " unbind from play service");
            context.unbindService(conn);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void doLock() {
        Log.i(TAG, " doLock ");
        try {
            lock.lockInterruptibly();
            condition.await(2, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }


    public boolean isPlay() {
        boolean isPlay = false;
        if (mService != null) {
            try {
                int type = mService.getPlayType();
                isPlay = mService.isPlaying(type);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return isPlay;
    }
}
