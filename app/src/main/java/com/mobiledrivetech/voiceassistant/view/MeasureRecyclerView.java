package com.mobiledrivetech.voiceassistant.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.mobiledrivetech.voiceassistant.hmi.picklist.BaseSizeAdapter;
import com.mobiledrivetech.voiceassistant.hmi.picklist.PickListManager;

import cerence.ark.assistant.api.ArkAssistant;
import cerence.ark.assistant.controller.event.PickListItemSelectedEvent;

public class MeasureRecyclerView extends RecyclerView {
    private static final String TAG = MeasureRecyclerView.class.getSimpleName();
    private int shortestDistance;
    private float downX = 0; // 手指按下的X軸座標
    private float slideDistance = 0; // 滑動的距離
    private float scrollX = 0; // X軸當前的位置

    public interface OnMeasureLisener {
        void onMeasure(int height, int width);
    }

    private OnMeasureLisener measureLisener;

    public void setMeasureLisener(OnMeasureLisener ls) {
        measureLisener = ls;
    }

    public MeasureRecyclerView(Context context) {
        super(context);
    }

    public MeasureRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public MeasureRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (measureLisener != null) {
            measureLisener.onMeasure(getMeasuredHeight(), getMeasuredWidth());
            measureLisener = null;
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        if (measureLisener != null) {
            measureLisener.onMeasure(getMeasuredHeight(), getMeasuredWidth());
            measureLisener = null;
        }
    }

    @Override
    public void onDraw(Canvas c) {
        super.onDraw(c);
//        LogUtils.dTag(TAG, "performance, Picklist rendering complete");
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        BaseSizeAdapter adapter = (BaseSizeAdapter) getAdapter();
        int currentPage = adapter.getCurrentPage();
        int totalPage = adapter.getTotalPage();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                downX = event.getX();
                break;
            case MotionEvent.ACTION_UP:
                slideDistance = event.getX() - downX;
                if (Math.abs(slideDistance) > shortestDistance) {
                    // 滑動距離足夠，執行翻頁
                    if (slideDistance > 0) {
                        // prePage
                        currentPage = currentPage == 1 ? 1 : currentPage - 1;
                        adapter.setSelectedPage(currentPage);
                    } else {
                        // nextPage
                        currentPage = currentPage == totalPage ? totalPage : currentPage + 1;
                        adapter.setSelectedPage(adapter.getCurrentPage() + 1);
                    }
                } else if (slideDistance == 0) {
                    View child = findChildViewUnder(event.getX(), event.getY());
                    ArkAssistant.get()
                            .sendAppEvent(
                                    new PickListItemSelectedEvent.Builder().setItemOrdinal(getChildAdapterPosition(child) + 1).build(),
                                    null);
                    PickListManager.get().hidePickList();
                }
                return true;
            default:
                break;
        }

        return super.onTouchEvent(event);
    }

    @Override
    protected void onMeasure(int widthSpec, int heightSpec) {
        shortestDistance = getMeasuredWidth() / 10;
        super.onMeasure(widthSpec, heightSpec);
    }

    @Override
    public void onScrolled(int dx, int dy) {
        scrollX += dx;
        super.onScrolled(dx, dy);
    }
}
