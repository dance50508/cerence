package com.mobiledrivetech.voiceassistant.utils;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DataUtils {
    private static final String TAG = DataUtils.class.getSimpleName();
    public static final int BUFFER_LEN = 8192;

    public static boolean copyData(Context context, String dataPath) {
        Log.d(TAG, String.format("data path = %s", dataPath));
        boolean isSuccess;
        String dmVersionFromAssets = readAssets2String(context, "data/version.txt");
        String dmVersionFromApp = readFile2String(dataPath + "/version.txt");
        if (!TextUtils.isEmpty(dmVersionFromAssets) && dmVersionFromAssets.equals(dmVersionFromApp)) {
            Log.d(TAG, String.format("dmVersionFromAssets = %s, dmVersionFromApp = %s", dmVersionFromAssets,
                    dmVersionFromApp));
            isSuccess = true;
        } else {
            Log.d(TAG, "copy data file begin");
            File file = new File(dataPath);
            boolean deleteResult = deleteDirOrFile(file);
            Log.d(TAG, "delete old data, result = " + deleteResult);
            //
            isSuccess = copyFileFromAssets(context, "data", dataPath);
            Log.d(TAG, String.format("dmVersionFromAssets = %s, result = %s", dmVersionFromAssets, isSuccess));
        }
        return isSuccess;
    }

    private static String readAssets2String(Context context, String assetsFilePath) {
        InputStream inputStream = null;
        ByteArrayOutputStream outputStream = null;
        String result = null;
        try {
            inputStream = context.getApplicationContext().getAssets().open(assetsFilePath);
            outputStream = new ByteArrayOutputStream();
            byte[] b = new byte[BUFFER_LEN];
            int len;
            while ((len = inputStream.read(b, 0, BUFFER_LEN)) != -1) {
                outputStream.write(b, 0, len);
            }
            result = new String(outputStream.toByteArray());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result == null ? "" : result;
    }

    private static String readFile2String(String filePath) {
        if (TextUtils.isEmpty(filePath)) {
            return null;
        }
        File file = new File(filePath);
        if (file == null || !file.exists() || file.isDirectory()) {
            return null;
        }
        InputStream inputStream = null;
        ByteArrayOutputStream outputStream = null;
        String result = null;
        try {
            inputStream = new FileInputStream(file);
            outputStream = new ByteArrayOutputStream();
            byte[] b = new byte[BUFFER_LEN];
            int len;
            while ((len = inputStream.read(b, 0, BUFFER_LEN)) != -1) {
                outputStream.write(b, 0, len);
            }
            result = new String(outputStream.toByteArray());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result == null ? "" : result;
    }

    public static boolean deleteDirOrFile(final File file) {
        if (file == null) {
            return false;
        }
        if (file.isDirectory()) {
            return deleteDir(file);
        }
        return deleteFile(file);
    }

    public static boolean deleteDir(final File dir) {
        if (dir == null) {
            return false;
        }
        if (!dir.exists()) {
            return true;
        }
        if (!dir.isDirectory()) {
            return false;
        }
        File[] files = dir.listFiles();
        if (files != null && files.length != 0) {
            for (File file : files) {
                if (file.isFile()) {
                    if (!file.delete()) {
                        return false;
                    }
                } else if (file.isDirectory()) {
                    if (!deleteDir(file)) {
                        return false;
                    }
                }
            }
        }
        return dir.delete();
    }

    public static boolean deleteFile(final File file) {
        return file != null && (!file.exists() || file.isFile() && file.delete());
    }

    public static boolean copyFileFromAssets(Context context, final String assetsFilePath, final String destFilePath) {
        boolean res = true;
        try {
            String[] assets = context.getApplicationContext().getAssets().list(assetsFilePath);
            if (assets != null && assets.length > 0) {
                for (String asset : assets) {
                    res &= copyFileFromAssets(context, assetsFilePath + "/" + asset, destFilePath + "/" + asset);
                }
            } else {
                res = writeFileFromIS(new File(destFilePath), context.getAssets().open(assetsFilePath));
            }
        } catch (IOException e) {
            e.printStackTrace();
            res = false;
        }
        return res;
    }

    private static boolean writeFileFromIS(final File file, final InputStream is) {
        if (!createOrExistsFile(file) || is == null) {
            return false;
        }
        OutputStream os = null;
        try {
            os = new FileOutputStream(file);
            byte[] data = new byte[BUFFER_LEN];
            int len;
            while ((len = is.read(data, 0, BUFFER_LEN)) != -1) {
                os.write(data, 0, len);
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (os != null) {
                    os.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static boolean createOrExistsFile(final File file) {
        if (file == null) {
            return false;
        }
        if (file.exists()) {
            return file.isFile();
        }
        if (!createOrExistsDir(file.getParentFile())) {
            return false;
        }
        try {
            return file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private static boolean createOrExistsDir(final File file) {
        return file != null && (file.exists() ? file.isDirectory() : file.mkdirs());
    }
}
