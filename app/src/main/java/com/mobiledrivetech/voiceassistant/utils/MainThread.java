package com.mobiledrivetech.voiceassistant.utils;

import android.os.Handler;
import android.os.Looper;

public class MainThread {
    private static Handler mHandler = new Handler(Looper.getMainLooper());

    public static synchronized void run(Runnable runnable) {
        mHandler.post(runnable);
    }

    public static synchronized void run(Runnable runnable, int delayMillis) {
        mHandler.postDelayed(runnable, delayMillis);
    }
}
