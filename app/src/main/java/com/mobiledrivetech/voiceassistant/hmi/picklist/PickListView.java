package com.mobiledrivetech.voiceassistant.hmi.picklist;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mobiledrivetech.voiceassistant.R;
import com.mobiledrivetech.voiceassistant.view.MeasureRecyclerView;


/**
 * Defines custom pick list view component to handle page events.
 */
public class PickListView extends LinearLayout {
    protected Context mContext;
    protected MeasureRecyclerView mRecyclerView;
    private LinearLayout mPageItem;

    public PickListView(Context context) {
        this(context, null);
    }

    public PickListView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PickListView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initView();
    }

    /**
     * init custom view
     */
    private void initView() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.view_pick_list, this, true);
        mRecyclerView = view.findViewById(R.id.rv_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        // mRecyclerView.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL));
        mRecyclerView.setLayoutManager(layoutManager);

        mPageItem = view.findViewById(R.id.list_page_item);
    }

    public void setAdapter(BaseSizeAdapter adapter, @Nullable Integer customHeight) {
        if (null != adapter) {
            if (null != customHeight) {
                adapter.setItemHeight(customHeight);
            } else {
                int measureHeight = mRecyclerView.getMeasuredHeight();
                if (measureHeight > 0) {
                    adapter.setItemHeight(measureHeight / adapter.getNumberPerPage());
                } else {
                    mRecyclerView.setMeasureLisener(
                            (height, width) -> adapter.setItemHeight(height / adapter.getNumberPerPage()));
                }
            }
        }
        mRecyclerView.setAdapter(adapter);
        /*mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull @NotNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    LinearLayoutManager layoutManager = (LinearLayoutManager)recyclerView.getLayoutManager();
                }
            }

            @Override
            public void onScrolled(@NonNull @NotNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });*/
    }


    /**
     * update ui like page num and arrow
     *
     * @param curPage
     */
    public void updatePageUi(int curPage, int totalPage) {
        mPageItem.removeAllViews();
        for (int i = 0; i < totalPage; i++) {
            ImageView imageView = new ImageView(mContext);
            if (i == curPage - 1) {
                imageView.setImageResource(R.drawable.ic_page_selected);
            } else {
                imageView.setImageResource(R.drawable.ic_page_not_selected);
            }
            mPageItem.addView(imageView);
            final int b = i;
            View pageItem = mPageItem.getChildAt(i);
            float scale = getResources().getDisplayMetrics().density;
            int dpAsPixels = (int) (10 * scale + 0.5f);
            pageItem.setPadding(dpAsPixels, dpAsPixels, dpAsPixels, dpAsPixels);
            pageItem.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((BaseSizeAdapter) mRecyclerView.getAdapter()).setSelectedPage(b + 1);
                }
            });
        }
    }
}
