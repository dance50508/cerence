package com.mobiledrivetech.voiceassistant.hmi.picklist;

import android.app.Application;
import android.util.Log;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.mobiledrivetech.voiceassistant.R;

import cerence.ark.assistant.controller.struct.PlaceInfo;
import cerence.ark.assistant.library.Utils;
import skin.support.content.res.SkinCompatResources;

public class PoiListAdapter extends BaseSizeAdapter<PlaceInfo> {
    public PoiListAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, PlaceInfo item) {
        super.convert(helper, item);
        if (mCurrentSelectedItem == helper.getLayoutPosition()) {
            helper.itemView.setBackgroundColor(SkinCompatResources.getInstance().getColor(R.color.color_highlight));
        } else {
            helper.itemView.setBackground(null);
        }
        Log.d("TAG", "convert: " + (helper.getLayoutPosition() + 1) + "\n" + item.getName() + "\n" + item.getAddress());
        helper.setText(R.id.tv_order, String.valueOf(helper.getLayoutPosition() + 1));
        helper.setTextColor(R.id.tv_order, SkinCompatResources.getInstance().getColor(R.color.color_item_text_view));
        helper.setText(R.id.tv_title, item.getName());
        helper.setTextColor(R.id.tv_title, SkinCompatResources.getInstance().getColor(R.color.color_item_text_view));
        helper.setText(R.id.tv_desc, item.getAddress());
        helper.setTextColor(R.id.tv_desc, SkinCompatResources.getInstance().getColor(R.color.color_item_text_view));
        helper.setText(R.id.tv_km, item.getDistance()+"km");
        helper.setTextColor(R.id.tv_km, SkinCompatResources.getInstance().getColor(R.color.color_item_text_view));
    }
}
