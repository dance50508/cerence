package com.mobiledrivetech.voiceassistant.hmi.picklist;

import android.util.Log;

import androidx.core.content.ContextCompat;

import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.mobiledrivetech.voiceassistant.R;

import cerence.ark.assistant.controller.struct.PhoneNumberInfo;
import skin.support.content.res.SkinCompatResources;

public class PhoneNumListAdapter extends BaseSizeAdapter<PhoneNumberInfo> {
    public PhoneNumListAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, PhoneNumberInfo item) {
        super.convert(helper, item);
        if (mCurrentSelectedItem == helper.getLayoutPosition()) {
            helper.itemView.setBackgroundColor(SkinCompatResources.getInstance().getColor(R.color.color_highlight));
        } else {
            helper.itemView.setBackground(null);
        }
        helper.setText(R.id.tv_order, String.valueOf(helper.getLayoutPosition() + 1));
        helper.setTextColor(R.id.tv_order, SkinCompatResources.getInstance().getColor(R.color.color_item_text_view));
        helper.setText(R.id.tv_title, item.getNumber());
        helper.setTextColor(R.id.tv_title, SkinCompatResources.getInstance().getColor(R.color.color_item_text_view));
        helper.setText(R.id.tv_desc, "");
        helper.setTextColor(R.id.tv_desc, SkinCompatResources.getInstance().getColor(R.color.color_item_text_view));
    }
}
