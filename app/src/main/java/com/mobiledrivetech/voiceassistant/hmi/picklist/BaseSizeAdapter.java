package com.mobiledrivetech.voiceassistant.hmi.picklist;

import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseSizeAdapter<T> extends BaseQuickAdapter<T, BaseViewHolder> {
    // the default display item count per page.
    private int DEFAULT_PAGE_SIZE = 4;
    // display item count per page.
    private int mPageSize = DEFAULT_PAGE_SIZE;
    // the current page.
    private int mCurrentPage = 1;
    // the total page count
    private int mTotalPage = 1;
    private int mItemHight = 0;
    protected int mItemHeight = 0;
    protected int mItemWidth = 0;
    protected int mStartIndex = 0;
    protected int mEndIndex = 0;
    protected T t;
    // all of data
    protected List<T> mAllData = new ArrayList<>();
    // the current page data.
    protected List<T> mCurrentDisplayData = new ArrayList<>();
    // the high light item index.
    protected int mCurrentSelectedItem = -1;
    protected UpdateUiListener mUpdateUiListener;

    public BaseSizeAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, T item) {
        ViewGroup.LayoutParams layoutParams = helper.itemView.getLayoutParams();
        if (mItemHeight > 0) {
            layoutParams.height = mItemHeight;
        }
        if (mItemWidth > 0) {
            layoutParams.width = mItemWidth;
        }
    }

    public void setItemHeight(int height) {
        mItemHeight = height;
        notifyDataSetChanged();
    }

    public void setItemWidth(int width) {
        mItemWidth = width;
        notifyDataSetChanged();
    }

    public void setItemHeightAndWidth(int height, int width) {
        mItemHeight = height;
        mItemWidth = width;
        notifyDataSetChanged();
    }

    public void setDefaultPageSize(int defaultPageSize) {
        this.DEFAULT_PAGE_SIZE = defaultPageSize;
        this.mPageSize = defaultPageSize;
    }

    public int getCurrentPage() {
        return mCurrentPage;
    }

    public int getStartIndex() {
        return mStartIndex;
    }

    public int getNumberPerPage() {
        return mPageSize;
    }

    public int getTotalPage() {
        return mTotalPage;
    }

    public void setSelectedPage(int page) {
        if (page < 1 || page > mTotalPage) {
            return;
        }
        mCurrentPage = page;
        setPage(page, mPageSize);
    }

    private void setPage(int page, int pageSize) {
        mCurrentDisplayData.clear();
        mCurrentDisplayData.addAll(getPageData(page, pageSize));
        setList(mCurrentDisplayData);
        //        setNewInstance(mCurrentDisplayData);
        if (null != mUpdateUiListener) {
            mUpdateUiListener.updateUi(mCurrentPage, mTotalPage);
        }
    }

    public void setSelectedItem(int index) {
        mCurrentSelectedItem = index;
        notifyDataSetChanged();
    }

    public void setAllData(List<T> allData) {
        mCurrentSelectedItem = -1;
        if (null == allData) {
            return;
        }
        mAllData.clear();
        mAllData.addAll(allData);
        calculateTotalPage();
        setSelectedPage(1);
    }

    private void calculateTotalPage() {
        if (mAllData.size() % mPageSize > 0) {
            mTotalPage = mAllData.size() / mPageSize + 1;
        } else {
            mTotalPage = mAllData.size() / mPageSize;
        }
    }

    public List<T> getAllData() {
        return mAllData;
    }

    private List<T> getPageData(int page, int pageSize) {
        int index = page - 1;
        if (mAllData.size() == 0 || mAllData.size() <= pageSize) {
            return mAllData;
        }
        int nTotalPage = mAllData.size() / pageSize;
        int nRestNum = mAllData.size() % pageSize;
        if (index < nTotalPage) {
            int max = -1;
            if ((index + 1) * pageSize >= mAllData.size()) {
                max = mAllData.size();
            } else {
                max = (index + 1) * pageSize;
            }
            mEndIndex = max;
            mStartIndex = index * pageSize;
            return mAllData.subList(mStartIndex, mEndIndex);
        } else if (index == nTotalPage) {
            if (nRestNum == 0) {
                mEndIndex = index * pageSize;
                mStartIndex = (index - 1) * pageSize;
                return mAllData.subList(mStartIndex, mEndIndex);
            } else {
                mEndIndex = index * pageSize + nRestNum;
                mStartIndex = index * pageSize;
                return mAllData.subList(mStartIndex, mEndIndex);
            }
        } else {
            return new ArrayList<>();
        }
    }

    public void setUpdateUiListener(UpdateUiListener listener) {
        mUpdateUiListener = listener;
    }

    public interface UpdateUiListener {
        void updateUi(int curPage, int totalPage);
    }

    public boolean hasPrePage() {
        return mCurrentPage != 1;
    }

    public boolean hasNextPage() {
        return mCurrentPage < mTotalPage;
    }
}
