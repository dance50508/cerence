package com.mobiledrivetech.voiceassistant.hmi;

public @interface HmiEnum {
    String DISPLAY = "display";
    String PAGE_CONTROL = "pageControl";
    String SHOW_PICK_LIST = "showPicklist";
    String HIGH_LIGHT_ITEM = "highlightItem";
}
