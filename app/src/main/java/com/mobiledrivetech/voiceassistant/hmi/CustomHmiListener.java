/*
 * Copyright (c) 2021 Cerence, Inc. All rights reserved.
 */

package com.mobiledrivetech.voiceassistant.hmi;

import android.util.Log;

import com.mobiledrivetech.voiceassistant.bean.BaseBean;
import com.mobiledrivetech.voiceassistant.bean.WeatherBean;
import com.mobiledrivetech.voiceassistant.constants.VoiceConstants;
import com.mobiledrivetech.voiceassistant.eventbus.MessageInfo;
import com.mobiledrivetech.voiceassistant.hmi.picklist.PickListManager;
import com.mobiledrivetech.voiceassistant.voiceui.VoiceUIManager;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import cerence.ark.assistant.api.BaseResult;
import cerence.ark.assistant.api.IRequest;
import cerence.ark.assistant.api.IResponseSender;
import cerence.ark.assistant.api.Response;
import cerence.ark.assistant.api.hmi.HmiListener;
import cerence.ark.assistant.api.notification.AsrResultUpdated;
import cerence.ark.assistant.api.notification.DialogEvent;
import cerence.ark.assistant.api.notification.DialogStateChanged;
import cerence.ark.assistant.api.notification.INotification;
import cerence.ark.assistant.api.notification.SpeechLevelUpdated;
import cerence.ark.assistant.controller.domain.hmi.param.DisplayParam;
import cerence.ark.assistant.controller.domain.hmi.param.HighlightItemParam;
import cerence.ark.assistant.controller.domain.hmi.param.PageControlParam;
import cerence.ark.assistant.controller.domain.hmi.param.ShowPicklistParam;
import cerence.ark.assistant.controller.enumeration.ResultCodeEnum;

/**
 * Defines a custom hmi listener to receive hmi request and notification message.
 * <p>
 * explanation:
 * 1. hmi request {@link CustomHmiListener#onRequest(IRequest, IResponseSender)}:
 * there are four types of request messages:
 * a) {@link HmiEnum#DISPLAY} means that the request message {@code request} can be displayed according to wishes.
 * b) {@link HmiEnum#SHOW_PICK_LIST} means that the request message {@code request} should be displayed as a pick list.
 * c) {@link HmiEnum#HIGH_LIGHT_ITEM} the premise of receiving this message is that the current display is a pick list,
 * then mark which item in the pick list should be highlighted.
 * d) {@link HmiEnum#PAGE_CONTROL} receive this message then switch the pick list to the specified page.
 * <p>
 * 2. hmi notification {@link CustomHmiListener#onNotification(INotification)}:
 * there are four types of notification messages:
 * a) {@link AsrResultUpdated} Notification of voice recognition result
 * b) {@link DialogEvent} Notification of dialog event , see also {@link DialogEvent.Event}
 * c) {@link SpeechLevelUpdated} Notification of speech level, Range is 0 to 100.
 * d) {@link DialogStateChanged} Notification of dialog state. see also {@link DialogStateChanged.State}
 */
public class CustomHmiListener implements HmiListener {
    private static final String TAG = CustomHmiListener.class.getSimpleName();
    private String inputString = "";
    private int widgetType = VoiceConstants.WidgetType.TYPE_OUTPUT;
    private WeatherBean weatherBean = null;

    @Override
    public void onRequest(IRequest request, IResponseSender<BaseResult> responseSender) {
        // send message to UI
        MessageInfo messageEvent = new MessageInfo();
        messageEvent.setJson(request.getParams());
        EventBus.getDefault().post(messageEvent);
        if (null == request) {
            Log.e(TAG, "onRequest request is null");
            return;
        }
        Log.d(TAG, "onRequest name " + request.getName());
        switch (request.getName()) {
            case HmiEnum.DISPLAY: {
                try {
                    DisplayParam displayParam = DisplayParam.fromJson(request.getParams());
                    Log.d(TAG, "onRequest displayParam " + displayParam.toJsonString());
                    switch (displayParam.getScenario()) {
                        case HmiDisplayEnum.WEATHER_INFORMATION:
                            JSONObject currentInfo = displayParam.getData().optJSONObject("current_info");
                            if (currentInfo != null) {
                                widgetType = VoiceConstants.WidgetType.TYPE_WIDGET_WEATHER;
                                weatherBean = new WeatherBean();
                                weatherBean.setType(VoiceConstants.WidgetType.TYPE_WIDGET_WEATHER);
                                weatherBean.setWeather(currentInfo.getString("phenomena"));
                                weatherBean.setCurrentWeatherIcon("ic_" + currentInfo.getString("weather_code"));
                                weatherBean.setCity(currentInfo.getString("city"));
                                weatherBean.setLowTemperature(currentInfo.getString("temperature"));
                                weatherBean.setHighTemperature(currentInfo.getString("temperature"));
                                VoiceUIManager.getInstance().updateUI(weatherBean);
                            }
                            break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                responseSender.send(new Response<BaseResult>(ResultCodeEnum.OK));
            }
            break;
            case HmiEnum.PAGE_CONTROL: {
                try {
                    PageControlParam pageControlParam = PageControlParam.fromJson(request.getParams());
                    PickListManager.get().pageControl(pageControlParam, responseSender);
                    Log.d(TAG, "onRequest pageControlParam " + pageControlParam.toJsonString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            break;
            case HmiEnum.SHOW_PICK_LIST: {
                try {
                    ShowPicklistParam showPicklistParam = ShowPicklistParam.fromJson(request.getParams());
                    Log.d(TAG, "onRequest showPicklistParam " + showPicklistParam.toJsonString());
                    pickListShow(showPicklistParam, responseSender);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            break;
            case HmiEnum.HIGH_LIGHT_ITEM: {
                try {
                    HighlightItemParam highlightItemParam = HighlightItemParam.fromJson(request.getParams());
                    PickListManager.get().setSelectedItem(highlightItemParam.getItemOrdinal());
                    Log.d(TAG, "onRequest highlightItemParam " + highlightItemParam.toJsonString());
                    responseSender.send(new Response<>(ResultCodeEnum.OK));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            break;
            default:
                break;
        }
    }

    private void pickListShow(ShowPicklistParam param, IResponseSender response) {
        PickListManager.get().showPickList(param.getScenario(), param.getData().toString(), response);
    }

    @Override
    public void onNotification(INotification notification) {
        if (notification instanceof AsrResultUpdated) {
            AsrResultUpdated asrResultUpdated = (AsrResultUpdated) notification;
            boolean isEnableWordStreaming = true;
            if (isEnableWordStreaming) {
                //VoiceBarManager.get().setRecognizedContent(asrResultUpdated.getUtterance());
                BaseBean baseBean = new BaseBean();
                widgetType = VoiceConstants.WidgetType.TYPE_INPUT;
                baseBean.setType(VoiceConstants.WidgetType.TYPE_INPUT);
                inputString = asrResultUpdated.getUtterance();
                baseBean.setUtterance(inputString);
                VoiceUIManager.getInstance().updateUI(baseBean);
            } else {
                if (asrResultUpdated.isFinal()) {
                    //VoiceBarManager.get().setRecognizedContent(asrResultUpdated.getUtterance());
                    BaseBean baseBean = new BaseBean();
                    baseBean.setType(VoiceConstants.WidgetType.TYPE_INPUT);
                    baseBean.setUtterance(asrResultUpdated.getUtterance());
                    VoiceUIManager.getInstance().updateUI(baseBean);
                }
            }
            Log.d(TAG, "getUtterance = " + asrResultUpdated.getUtterance());
        } else if (notification instanceof DialogEvent) {
            DialogEvent dialogEvent = (DialogEvent) notification;
            //VoiceBarManager.get().setDialogEvent(dialogEvent.getEvent());

            Log.d(TAG, "dialogEvent = " + dialogEvent.getEvent());

            EventBus.getDefault().post(dialogEvent);
            //PROMPT_BEGIN is when sdk start to talk, and ends with PROMPT_END
            if (DialogEvent.Event.PROMPT_BEGIN.equals(dialogEvent.getEvent())) {
                if (widgetType == VoiceConstants.WidgetType.TYPE_INPUT) {
                    BaseBean baseBean = new BaseBean();
                    baseBean.setType(VoiceConstants.WidgetType.TYPE_OUTPUT);
                    baseBean.setUtterance(inputString);
                    VoiceUIManager.getInstance().updateUI(baseBean);
                } else if (widgetType == VoiceConstants.WidgetType.TYPE_WIDGET_WEATHER) {
                    VoiceUIManager.getInstance().updateUI(weatherBean);
                    weatherBean = null;
                }
                try {
                    JSONObject extra = new JSONObject(dialogEvent.getExtra());
                    String hints = extra.optString("hints");
                    String text = extra.optString("text");
                    Log.d(TAG, "hints " + hints);
                    Log.d(TAG, "text " + text);
                    EventBus.getDefault().post(hints);
                    EventBus.getDefault().post(text);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (DialogEvent.Event.SESSION_END.equals(dialogEvent.getEvent()) && widgetType != VoiceConstants.WidgetType.TYPE_WIDGET_WEATHER) {
                Log.d(TAG, "onNotification: SESSION_END and is not TYPE_WIDGET_WEATHER");
                VoiceUIManager.getInstance().setIsInSession(false);
                VoiceUIManager.getInstance().hideFloatWindow();
            } else if (DialogEvent.Event.SESSION_BEGIN.equals(dialogEvent.getEvent())) {
                Log.d(TAG, "onNotification: SESSION_BEGIN");
                VoiceUIManager.getInstance().setIsInSession(true);
            }
        } else if (notification instanceof SpeechLevelUpdated) {
            SpeechLevelUpdated levelUpdated = (SpeechLevelUpdated) notification;
            Log.d(TAG, "levelUpdated = " + levelUpdated.getSpeechLevel());

            //VoiceBarManager.get().updateSoundLevel(levelUpdated.getSpeechLevel());
        } else if (notification instanceof DialogStateChanged) {
            DialogStateChanged dialogStateChanged = (DialogStateChanged) notification;
            //VoiceBarManager.get().setDialogState(dialogStateChanged.getState());
            VoiceUIManager.getInstance().dialogStateChanged(dialogStateChanged.getState());
            Log.d(TAG, "dialogStateChanged = " + dialogStateChanged.getState());
            EventBus.getDefault().post(dialogStateChanged);
        }
    }
}
