package com.mobiledrivetech.voiceassistant.hmi.picklist;

import androidx.annotation.Nullable;

public interface IControlPickList {
    void showPickList(BaseSizeAdapter adapter, @Nullable Integer customHeight);

    void hidePickList();
}
