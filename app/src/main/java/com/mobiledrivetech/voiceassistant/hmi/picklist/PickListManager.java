package com.mobiledrivetech.voiceassistant.hmi.picklist;

import android.util.Log;

import com.mobiledrivetech.voiceassistant.R;
import com.mobiledrivetech.voiceassistant.hmi.HmiDisplayEnum;

import org.json.JSONException;

import cerence.ark.assistant.api.BaseResult;
import cerence.ark.assistant.api.IResponseSender;
import cerence.ark.assistant.api.Response;
import cerence.ark.assistant.controller.domain.hmi.param.NaviPoiListParam;
import cerence.ark.assistant.controller.domain.hmi.param.PageControlParam;
import cerence.ark.assistant.controller.domain.hmi.param.PhoneCallLogParam;
import cerence.ark.assistant.controller.domain.hmi.param.PhoneContactDisambigListParam;
import cerence.ark.assistant.controller.domain.hmi.param.PhoneContactListParam;
import cerence.ark.assistant.controller.domain.hmi.param.PhoneNumberDisambigListParam;
import cerence.ark.assistant.controller.domain.hmi.result.PageControlResult;
import cerence.ark.assistant.controller.domain.hmi.result.ShowPicklistResult;
import cerence.ark.assistant.controller.enumeration.PageControlTypeEnum;
import cerence.ark.assistant.controller.enumeration.ResultCodeEnum;
import cerence.ark.assistant.controller.struct.PageInfo;

/**
 * Defines pick list view control manager to handle the parsing of parameters and control the pick list .
 */
public class PickListManager {
    private String TAG = PickListManager.class.getSimpleName();
    IControlPickList controlPicklist;
    BaseSizeAdapter adapter;

    private PickListManager() {
    }

    public static PickListManager get() {
        return SingletonHolder.INSTANCE;
    }

    private static class SingletonHolder {
        private static final PickListManager INSTANCE = new PickListManager();
    }

    public void setControlPicklist(IControlPickList list) {
        controlPicklist = list;
    }

    public void showPickList(String scenario, String data, IResponseSender<BaseResult> responseSender) {
        adapter = null;
        Log.d(TAG, "showPickList " + scenario);
        switch (scenario) {
            case HmiDisplayEnum.PHONE_CONTACT_LIST:
                try {
                    PhoneContactListParam param = PhoneContactListParam.fromJson(data);
                    if (null != controlPicklist) {
                        adapter = new ContactListAdapter(R.layout.item_common);
                        adapter.setDefaultPageSize(6);
                        controlPicklist.showPickList(adapter, 80);
                        adapter.setAllData(param.getContactList());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case HmiDisplayEnum.PHONE_CALL_LOG:
                try {
                    PhoneCallLogParam param = PhoneCallLogParam.fromJson(data);
                    if (null != controlPicklist) {
                        adapter = new CallLogListAdapter(R.layout.item_common);
                        controlPicklist.showPickList(adapter, null);
                        adapter.setAllData(param.getCallLogList());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case HmiDisplayEnum.PHONE_CONTACT_DISAMBIG_LIST:
                try {
                    PhoneContactDisambigListParam param = PhoneContactDisambigListParam.fromJson(data);
                    if (null != controlPicklist) {
                        adapter = new ContactListAdapter(R.layout.item_common);
                        controlPicklist.showPickList(adapter, null);
                        adapter.setAllData(param.getContactList());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case HmiDisplayEnum.PHONE_NUMBER_DISAMBIG_LIST:
                try {
                    PhoneNumberDisambigListParam param = PhoneNumberDisambigListParam.fromJson(data);
                    if (null != controlPicklist) {
                        adapter = new PhoneNumListAdapter(R.layout.item_common);
                        controlPicklist.showPickList(adapter, null);
                        adapter.setAllData(param.getPhoneNumberList());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case HmiDisplayEnum.NAVI_POI_LIST:
                try {
                    NaviPoiListParam param = NaviPoiListParam.fromJson(data);
                    Log.d(TAG, "list size " + param.getPoiList().size());
                    if (param.getPoiList().size() > 0) {
                        Log.d(TAG, "list size " + param.getPoiList().get(0).getAddress());
                    }
                    if (null != controlPicklist) {
                        adapter = new PoiListAdapter(R.layout.item_common_navigate);
                        controlPicklist.showPickList(adapter, 90);
                        adapter.setAllData(param.getPoiList());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            default:
                Log.e("PickListManager", "pickListShow unhandler scenario = " + scenario);
                break;
        }
        if (null != adapter) {
            ShowPicklistResult result = new ShowPicklistResult();
            PageInfo pageInfo = new PageInfo();
            pageInfo.setStartItemIndex(adapter.getStartIndex());
            pageInfo.setStartItemOrdinal(1);
            pageInfo.setNumOfItems(adapter.getNumberPerPage());
            pageInfo.setTotalPageNumber(adapter.getTotalPage());
            result.setPageInfo(pageInfo);
            responseSender.send(new Response<>(ResultCodeEnum.OK, result));
        }
    }

    public void hidePickList() {
        Log.d(TAG, "hidePickList");
        if (null != controlPicklist) {
            controlPicklist.hidePickList();
        }
    }

    public void pageControl(PageControlParam pageControlParam, IResponseSender<BaseResult> responseSender) {
        Log.d(TAG, "pageControl " + pageControlParam.getPageControlType());
        switch (pageControlParam.getPageControlType()) {
            case PageControlTypeEnum.FIRST_PAGE:
                if (adapter.getCurrentPage() == 1) {
                    PageControlResult resp = new PageControlResult();
                    resp.setStartItemIndex(adapter.getStartIndex());
                    resp.setNumOfItems(adapter.getNumberPerPage());
                    resp.setStartItemOrdinal(1);
                    responseSender.send(new Response<>(ResultCodeEnum.ALREADY_EXPECTED, resp));
                } else {
                    executePageChange(1, responseSender);
                }
                break;
            case PageControlTypeEnum.LAST_PAGE:
                if (adapter.getCurrentPage() == adapter.getTotalPage()) {
                    PageControlResult resp = new PageControlResult();
                    resp.setStartItemIndex(adapter.getStartIndex());
                    resp.setNumOfItems(adapter.getNumberPerPage());
                    resp.setStartItemOrdinal(1);
                    responseSender.send(new Response<>(ResultCodeEnum.ALREADY_EXPECTED, resp));
                } else {
                    executePageChange(adapter.getTotalPage(), responseSender);
                }
                break;
            case PageControlTypeEnum.PREV_PAGE:
                if (adapter.getCurrentPage() > 1) {
                    executePageChange(adapter.getCurrentPage() - 1, responseSender);
                } else {
                    PageControlResult resp = new PageControlResult();
                    resp.setStartItemIndex(adapter.getStartIndex());
                    resp.setNumOfItems(adapter.getNumberPerPage());
                    resp.setStartItemOrdinal(1);
                    responseSender.send(new Response<>(ResultCodeEnum.OK, resp));
                }
                break;
            case PageControlTypeEnum.NEXT_PAGE:
                if (adapter.getCurrentPage() < adapter.getTotalPage()) {
                    executePageChange(adapter.getCurrentPage() + 1, responseSender);
                } else {
                    PageControlResult resp = new PageControlResult();
                    resp.setStartItemIndex(adapter.getStartIndex());
                    resp.setNumOfItems(adapter.getNumberPerPage());
                    resp.setStartItemOrdinal(1);
                    responseSender.send(new Response<>(ResultCodeEnum.OUT_OF_RANGE, resp));
                }
                break;
            case PageControlTypeEnum.SET_PAGE:
                int pageOrdinal = pageControlParam.getPageOrdinal();
                if (adapter.getCurrentPage() == pageOrdinal) {
                    PageControlResult resp = new PageControlResult();
                    resp.setStartItemIndex(adapter.getStartIndex());
                    resp.setNumOfItems(adapter.getNumberPerPage());
                    resp.setStartItemOrdinal(1);
                    responseSender.send(new Response<>(ResultCodeEnum.ALREADY_EXPECTED, resp));
                } else {
                    executePageChange(pageOrdinal, responseSender);
                }
                break;
            default:
                responseSender.send(new Response<>(ResultCodeEnum.ERROR));
                break;
        }
    }

    public void setSelectedItem(int itemOrdinal) {
        adapter.setSelectedItem(itemOrdinal - 1);
    }

    private void executePageChange(int page, IResponseSender<BaseResult> responseSender) {
        adapter.setSelectedPage(page);
        PageControlResult resp = new PageControlResult();
        resp.setStartItemIndex(adapter.getStartIndex());
        resp.setNumOfItems(adapter.getNumberPerPage());
        resp.setStartItemOrdinal(1);
        responseSender.send(new Response<>(ResultCodeEnum.OK, resp));
    }
}
