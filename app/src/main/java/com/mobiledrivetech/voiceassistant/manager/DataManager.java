package com.mobiledrivetech.voiceassistant.manager;

public class DataManager {
    private boolean mIsEnableWordStreaming = false;

    private static final class SingletonHolder {
        private static final DataManager INSTANCE = new DataManager();
    }

    public static DataManager get() {
        return SingletonHolder.INSTANCE;
    }

    public boolean getEnableWordStreaming() {
        return mIsEnableWordStreaming;
    }

    public void setEnableWordStreaming(boolean enableWordStreaming) {
        mIsEnableWordStreaming = enableWordStreaming;
    }
}
