package com.mobiledrivetech.voiceassistant.manager;

import android.Manifest;
import android.database.Cursor;
import android.os.Build;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cerence.ark.assistant.controller.enumeration.CallLogTypeEnum;
import cerence.ark.assistant.controller.enumeration.PhoneNumberTypeEnum;
import cerence.ark.assistant.controller.struct.CallLogInfo;
import cerence.ark.assistant.controller.struct.ContactInfo;
import cerence.ark.assistant.controller.struct.PhoneNumberInfo;
import cerence.ark.assistant.library.Utils;

/**
 *
 */
public class PhoneManager {
    private static final String TAG = PhoneManager.class.getSimpleName();
    private static final int MAX_CALL_LOG_COUNT = 30;
    /**
     * Maps the data base number types to Cerence number types.
     */
    private final Map<Integer, String> mPhoneTypeMap = new HashMap<>(16);
    private final Map<String, List<Integer>> mCallLogTypeMap = new HashMap<>(16);
    private final String[] mProjectionContacts = new String[] {
            ContactsContract.RawContactsEntity.DATA_ID, ContactsContract.CommonDataKinds.StructuredName.CONTACT_ID,
            ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME };
    /**
     * Defines the projection values used when retrieving contact name information.
     */
    private final String[] mProjectionCallLog = new String[] {
            CallLog.Calls.CACHED_NAME, CallLog.Calls.NUMBER, CallLog.Calls.TYPE, CallLog.Calls.DATE,
            CallLog.Calls.CACHED_NUMBER_TYPE };

    public PhoneManager() {
        mPhoneTypeMap.put(ContactsContract.CommonDataKinds.Phone.TYPE_HOME, PhoneNumberTypeEnum.HOME);
        mPhoneTypeMap.put(ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE, PhoneNumberTypeEnum.MOBILE);
        mPhoneTypeMap.put(ContactsContract.CommonDataKinds.Phone.TYPE_WORK, PhoneNumberTypeEnum.WORK);
        // init ANSWERED_CALL_LOG
        List<Integer> list = new ArrayList<>();
        list.add(CallLog.Calls.INCOMING_TYPE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
            list.add(CallLog.Calls.ANSWERED_EXTERNALLY_TYPE);
        }
        mCallLogTypeMap.put(CallLogTypeEnum.ANSWERED_CALL_LOG, list);
        // init INCOMING_CALL_LOG
        list = new ArrayList<>();
        list.add(CallLog.Calls.INCOMING_TYPE);
        list.add(CallLog.Calls.MISSED_TYPE);
        mCallLogTypeMap.put(CallLogTypeEnum.INCOMING_CALL_LOG, list);
        // init MISSED_CALL_LOG
        list = new ArrayList<>();
        list.add(CallLog.Calls.MISSED_TYPE);
        list.add(CallLog.Calls.VOICEMAIL_TYPE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            list.add(CallLog.Calls.BLOCKED_TYPE);
            list.add(CallLog.Calls.REJECTED_TYPE);
        }
        mCallLogTypeMap.put(CallLogTypeEnum.MISSED_CALL_LOG, list);
        // init OUTGOING_CALL_LOG
        list = new ArrayList<>();
        list.add(CallLog.Calls.OUTGOING_TYPE);
        mCallLogTypeMap.put(CallLogTypeEnum.OUTGOING_CALL_LOG, list);
    }

    /**
     * The filter declaring which row[s] to return when querying for contacts, formatted as an SQL 'WHERE' clause
     * (excluding the 'WHERE' itself). Passing {@code null} returns all contacts.
     *
     * @return the contacts selection string.
     */
    protected String getContactsSelection() {
        return ContactsContract.Data.MIMETYPE + " = ? AND " + ContactsContract.Data.DELETED + " = ?";
    }

    /**
     * The arguments to the contacts selection string {@link #getContactsSelection()}. The value may be an empty array
     * if there are no arguments.
     *
     * @return the contacts selection arguments.
     */
    protected String[] getContactsSelectionArgs() {
        return new String[] { ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE, "0" };
    }

    /**
     * Loads the contact data of the specified device[s].
     *
     * @return the loaded contact[s] mapped by device.
     */
    public List<ContactInfo> getContacts() {
        List<ContactInfo> list = new ArrayList<>();
        if (!Utils.checkSelfPermission(Manifest.permission.READ_CONTACTS)) {
            Log.e(TAG, "loadContacts: the android.permission.READ_CONTACTS was denied.");
            return list;
        }
        try (Cursor cursor = Utils.getContext()
                                  .getContentResolver()
                                  .query(ContactsContract.RawContactsEntity.CONTENT_URI, mProjectionContacts,
                                          getContactsSelection(), getContactsSelectionArgs(), null)) {
            if (cursor == null) {
                Log.e(TAG, "loadContacts: query fail, cursor is null");
                return list;
            }
            Log.d(TAG, "loadContacts: begin, contact count = " + cursor.getCount());
            int indexDataId = cursor.getColumnIndex(ContactsContract.RawContactsEntity.DATA_ID);
            int indexContactId = cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.CONTACT_ID);
            int indexName = cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME);
            ContactInfo contact;
            while (cursor.moveToNext()) {
                if (cursor.isNull(indexDataId)) {
                    Log.w(TAG, "loadContacts: data is invalid, data_id: " + indexDataId);
                    continue;
                }
                contact = new ContactInfo();
                contact.setContactId(cursor.getInt(indexContactId));
                contact.setContactName(cursor.getString(indexName));
                list.add(contact);
            }
            Log.d(TAG, "loadContacts: finish, count = " + list.size());
        } catch (Exception e) {
            Log.e(TAG, "loadContacts: exception", e);
        }
        return list;
    }

    /**
     * Loads the call log data of the specified device.
     *
     * @return the loaded call logs.
     */
    public List<CallLogInfo> getCallLogsByType(@CallLogTypeEnum String callLogType) {
        if (!Utils.checkSelfPermission(Manifest.permission.READ_CALL_LOG)) {
            Log.e(TAG, "loadCallLogsByType: the android.permission.READ_CALL_LOG was denied.");
            return null;
        }
        String selection = null;
        String[] selectionArgs = null;
        if (!CallLogTypeEnum.ALL_CALL_LOG.equals(callLogType)) {
            List<Integer> typeList = mCallLogTypeMap.get(callLogType);
            if (typeList != null) {
                int size = typeList.size();
                List<String> list = new ArrayList<>();
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < size; i++) {
                    Integer integer = typeList.get(i);
                    list.add(String.valueOf(integer));
                    if (i == 0) {
                        builder.append(CallLog.Calls.TYPE + " = ?");
                    } else {
                        builder.append(" OR " + CallLog.Calls.TYPE + " = ?");
                    }
                }
                selection = builder.toString();
                selectionArgs = list.toArray(new String[0]);
            }
        }
        String sortOrder = CallLog.Calls.DEFAULT_SORT_ORDER + " LIMIT " + MAX_CALL_LOG_COUNT;
        try (Cursor cursor = Utils.getContext()
                                  .getContentResolver()
                                  .query(CallLog.Calls.CONTENT_URI, mProjectionCallLog, selection, selectionArgs,
                                          sortOrder)) {
            if (cursor == null) {
                Log.e(TAG, "loadCallLogsByType: query fail, cursor is null");
                return null;
            }
            Log.d(TAG, String.format("loadCallLogsByType: begin, callLogType = %s, count = %s", callLogType,
                    cursor.getCount()));
            List<CallLogInfo> list = new ArrayList<>();
            CallLogInfo callLogInfo;
            ContactInfo contact;
            PhoneNumberInfo phoneNumberInfo;
            int indexNumber = cursor.getColumnIndex(CallLog.Calls.NUMBER);
            int indexDate = cursor.getColumnIndex(CallLog.Calls.DATE);
            int indexCachedName = cursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
            int indexCachedNumberType = cursor.getColumnIndex(CallLog.Calls.CACHED_NUMBER_TYPE);
            while (cursor.moveToNext()) {
                callLogInfo = new CallLogInfo();
                callLogInfo.setTimestamp(cursor.getLong(indexDate));
                //Create contact
                contact = new ContactInfo();
                contact.setContactName(cursor.getString(indexCachedName));
                callLogInfo.setContact(contact);
                //Create phone number
                int type = cursor.getInt(indexCachedNumberType);
                String phoneNumberType = mPhoneTypeMap.containsKey(type)
                                         ? mPhoneTypeMap.get(type)
                                         : PhoneNumberTypeEnum.OTHER;
                phoneNumberInfo = new PhoneNumberInfo();
                phoneNumberInfo.setNumber(cursor.getString(indexNumber));
                phoneNumberInfo.setNumberType(phoneNumberType);
                callLogInfo.setPhoneNumber(phoneNumberInfo);
                //Add call info to list.
                list.add(callLogInfo);
            }
            Log.d(TAG, "loadCallLogsByType: finish, count = " + list.size());
            return list;
        } catch (Exception e) {
            Log.e(TAG, "loadCallLogsByType: exception", e);
        }
        return null;
    }

    public List<PhoneNumberInfo> getPhoneNumberListById(int contactId) {
        String selection = ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?";
        String[] selectionArgs = new String[] { String.valueOf(contactId) };
        try (Cursor cursor = Utils.getContext()
                                  .getContentResolver()
                                  .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, selection,
                                          selectionArgs, null)) {
            if (cursor == null) {
                Log.e(TAG, "loadPhoneNumberListById: query fail, cursor is null, contactId = " + contactId);
                return null;
            }
            Log.d(TAG, "loadPhoneNumberListById: begin, count = " + cursor.getCount());
            List<PhoneNumberInfo> list = new ArrayList<>();
            PhoneNumberInfo phoneNumberInfo;
            int indexPhoneNumber = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int indexType = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE);
            while (cursor.moveToNext()) {
                phoneNumberInfo = new PhoneNumberInfo();
                phoneNumberInfo.setNumber(cursor.getString(indexPhoneNumber));
                int type = cursor.getInt(indexType);
                String phoneNumberType = mPhoneTypeMap.containsKey(type)
                                         ? mPhoneTypeMap.get(type)
                                         : PhoneNumberTypeEnum.OTHER;
                phoneNumberInfo.setNumberType(phoneNumberType);
                list.add(phoneNumberInfo);
            }
            Log.d(TAG, "loadPhoneNumberListById: finish, count = " + list.size());
            return list;
        } catch (Exception e) {
            Log.e(TAG, "loadPhoneNumberListById: exception, contactId = " + contactId, e);
        }
        return null;
    }
}
