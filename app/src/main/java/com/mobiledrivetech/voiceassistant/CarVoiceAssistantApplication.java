package com.mobiledrivetech.voiceassistant;

import android.app.Application;
import android.content.ContentResolver;
import android.provider.Settings;

import androidx.appcompat.app.AppCompatDelegate;

import skin.support.SkinCompatManager;
import skin.support.app.SkinAppCompatViewInflater;
import skin.support.app.SkinCardViewInflater;
import skin.support.constraint.app.SkinConstraintViewInflater;
import skin.support.design.app.SkinMaterialViewInflater;

public class CarVoiceAssistantApplication extends Application {
    public static final String THEME_TYPE_DEF = "0";
    public static String ThemeType = THEME_TYPE_DEF;
    private static CarVoiceAssistantApplication sApplication;

    @Override
    public void onCreate() {
        super.onCreate();
        sApplication = this;
        ContentResolver mContentResolver = this.getContentResolver();
        ThemeType = Settings.Global.getString(mContentResolver, "current_theme");
        if(ThemeType ==null || ThemeType.length() == 0){
            ThemeType = THEME_TYPE_DEF;
        }
    }

    public static CarVoiceAssistantApplication getInstance() {
        return sApplication;
    }
}
