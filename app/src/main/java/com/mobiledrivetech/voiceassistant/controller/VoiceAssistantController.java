package com.mobiledrivetech.voiceassistant.controller;

import android.annotation.SuppressLint;

import com.mobiledrivetech.voiceassistant.customizedimpl.CustomDomain;
import com.mobiledrivetech.voiceassistant.customizedimpl.CustomMusicDomain;
import com.mobiledrivetech.voiceassistant.customizedimpl.CustomPhoneDomain;
import com.mobiledrivetech.voiceassistant.customizedimpl.CustomUDEDomain;
import com.mobiledrivetech.voiceassistant.customizedimpl.CustomVCDomain;
import com.mobiledrivetech.voiceassistant.customizedimpl.DemoCCDomain;
import com.mobiledrivetech.voiceassistant.customizedimpl.DemoSystemDomain;

import java.util.Map;

import cerence.ark.assistant.api.BaseResult;
import cerence.ark.assistant.api.Dhi;
import cerence.ark.assistant.api.IRequest;
import cerence.ark.assistant.api.Response;
import cerence.ark.assistant.api.controller.Domain;
import cerence.ark.assistant.api.controller.IHostController;
import cerence.ark.assistant.api.dynamic.DefaultDynamicContentProvider;
import cerence.ark.assistant.api.dynamic.IDynamicContentProvider;
import cerence.ark.assistant.controller.enumeration.ResultCodeEnum;

/**
 * Defines controller class to load custom domains that implement by yourself.
 * <p>
 * Expect to show:
 * 1. load a domain that only override one or several methods that does not up to your expectations or it does not
 * work well on the target system. see also {@link CustomPhoneDomain}
 * <p>
 * 2. load a domain that extends default implementation and has a new request that added by customer.
 * see also {@link CustomPhoneDomain1}
 * <p>
 * 3. load a re-implement 'phone' domain. see also {@link CustomPhoneDomain2}
 * <p>
 * 4. load a brand new custom domain. see also {@link CustomDomain}
 */
public class VoiceAssistantController implements IHostController {
    @Override
    public void loadDomains(Map<String, Domain> map, Dhi dhi) {
        //        map.put("^[a-zA-Z_]+$", new SimpleAllDomain(dhi));
        map.put("Phone", new CustomPhoneDomain(dhi));  //
        map.put("CC", new DemoCCDomain(dhi));
        map.put("Custom", new CustomDomain(dhi));
        map.put("System", new DemoSystemDomain(dhi));
        map.put("UDE", new CustomUDEDomain(dhi));
        map.put("Music", new CustomMusicDomain(dhi));
        map.put("VehicleControl", new CustomVCDomain(dhi));
    }

    @SuppressLint("MissingPermission")
    @Override
    public IDynamicContentProvider loadDynamicContentProvider() {
        return new DefaultDynamicContentProvider();
    }

    @Override
    public Response<BaseResult> onUnhandledRequest(IRequest request) {
        return new Response<>(ResultCodeEnum.OK);
    }
}