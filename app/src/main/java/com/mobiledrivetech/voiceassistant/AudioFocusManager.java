package com.mobiledrivetech.voiceassistant;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.os.Build;
import android.util.Log;

import java.lang.reflect.Method;

/**
 * 音频焦点处理工具类
 */
public class AudioFocusManager {
    private String TAG = "CarCerence_" + AudioFocusManager.class.getSimpleName();

    private Context mContext;
    private AudioManager mAudioManager;
    private AudioManager.OnAudioFocusChangeListener mAudioFocusChangeListener;
    private AudioFocusRequest mAudioFocusRequest;

    public AudioFocusManager(Context context) {
        mContext = context;
        mAudioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
    }

    /**
     * 请求音频焦点 设置监听
     *
     * @return
     */
    public int requestAudioFocus() {
        if (mAudioManager == null) {
            mAudioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        }
        if (mAudioFocusChangeListener == null) {
            mAudioFocusChangeListener = new AudioManager.OnAudioFocusChangeListener() {//监听器
                @Override
                public void onAudioFocusChange(int focusChange) {
                    Log.d(TAG, "onAudioFocusChange: " + focusChange);
                    switch (focusChange) {
                        case AudioManager.AUDIOFOCUS_GAIN:
                            Log.d(TAG, "onAudioFocusChange: AUDIOFOCUS_GAIN");
                            //当其他应用申请焦点之后又释放焦点会触发此回调
                            break;
                        case AudioManager.AUDIOFOCUS_LOSS:
                            Log.d(TAG, "onAudioFocusChange: AUDIOFOCUS_LOSS");
                            //长时间丢失焦点,当其他应用申请的焦点为AUDIOFOCUS_GAIN时，
                            break;
                        case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                            Log.d(TAG, "onAudioFocusChange: AUDIOFOCUS_LOSS_TRANSIENT");
                            //短暂性丢失焦点，当其他应用申请AUDIOFOCUS_GAIN_TRANSIENT或AUDIOFOCUS_GAIN_TRANSIENT_EXCLUSIVE时，
                            break;
                        case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                            //短暂性丢失焦点并作降音处理
                            Log.d(TAG, "onAudioFocusChange: AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK");
                            break;
                    }
                }
            };
        }

        int requestFocusResult = 0;
        if (mAudioManager != null) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                requestFocusResult = mAudioManager.requestAudioFocus(mAudioFocusChangeListener,
                        AudioManager.STREAM_MUSIC,
                        AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
                Log.d(TAG, "requestAudioFocus Android 8.0 之前 :  SDK_INT < 26 =" + requestFocusResult);
            } else {
                if (mAudioFocusRequest == null) {
                    //                    mAudioFocusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN)
                    mAudioFocusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN_TRANSIENT)
                            .setAudioAttributes(new AudioAttributes.Builder()
                                    .setUsage(AudioAttributes.USAGE_MEDIA)
                                    .setContentType(AudioAttributes.CONTENT_TYPE_MOVIE)
                                    .build())
                            .setAcceptsDelayedFocusGain(true)
                            .setOnAudioFocusChangeListener(mAudioFocusChangeListener)
                            .build();
                }
                requestFocusResult = mAudioManager.requestAudioFocus(mAudioFocusRequest);
                Log.d(TAG, "requestAudioFocus Android 8.0 以后 : SDK_INT >= 26 =" + requestFocusResult);
            }
        }
        return requestFocusResult;
    }

    /**
     * 1.暂停、播放完成或退到后台释放音频焦点
     * 2.应该先请求音频焦点
     */
    public int releaseAudioFocus() {
//        Log.d(TAG, Log.getStackTraceString(new RuntimeException("什么地方调用释放焦点了")));
        if (mAudioManager == null) {
            mAudioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        }

        int abandonFocusResult = 0;
        Log.d(TAG, "mAudioManager= "+mAudioManager +"mAudioFocusChangeListener= "+mAudioFocusChangeListener);
        if (mAudioManager != null && mAudioFocusChangeListener != null) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                abandonFocusResult = mAudioManager.abandonAudioFocus(mAudioFocusChangeListener);
                Log.d(TAG, "releaseAudioFocus: SDK_INT < 26 =" + abandonFocusResult);
            } else {
                if (mAudioFocusRequest != null) {
                    abandonFocusResult = mAudioManager.abandonAudioFocusRequest(mAudioFocusRequest);
                    Log.d(TAG, "releaseAudioFocus: SDK_INT >= 26 =" + abandonFocusResult);
                }
            }
        }
        return abandonFocusResult;
    }

    /*定制接口*/
    public String getAudioFocus(Context context) {
        String focusPackage = "";
        try {
            Log.i(TAG, " do getAudioFocus ");
            AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
            Class<AudioManager> cls = AudioManager.class;//首先还是必须得到这个对象的Class。
            Method getAudioFocusPackageName = cls.getDeclaredMethod("getCurrentAudioFocusPackageName");//得到执行的method
            getAudioFocusPackageName.setAccessible(true);
            focusPackage = getAudioFocusPackageName.invoke(am).toString();
            Log.i(TAG, " AudioFocusPackageName : " + focusPackage);
        } catch (Exception e) {
            Log.i(TAG, " Exception : " + e.toString() + "   AudioFocusPackageName : " + focusPackage);
            e.getStackTrace();
        }
        return focusPackage;
    }
}
