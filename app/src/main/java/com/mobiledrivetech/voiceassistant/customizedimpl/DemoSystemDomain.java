package com.mobiledrivetech.voiceassistant.customizedimpl;

import com.mobiledrivetech.voiceassistant.R;
import com.mobiledrivetech.voiceassistant.eventbus.FinalOperation;
import com.mobiledrivetech.voiceassistant.music.MusicController;

import org.greenrobot.eventbus.EventBus;

import cerence.ark.assistant.api.Dhi;
import cerence.ark.assistant.api.IResponseSender;
import cerence.ark.assistant.api.Response;
import cerence.ark.assistant.controller.domain.system.SystemDomain;
import cerence.ark.assistant.controller.domain.system.param.CheckPermissionParam;
import cerence.ark.assistant.controller.domain.system.param.GetMediaStateBeforeConversationParam;
import cerence.ark.assistant.controller.domain.system.result.CheckPermissionResult;
import cerence.ark.assistant.controller.domain.system.result.GetActiveMediaTypeBeforeConversationResult;
import cerence.ark.assistant.controller.domain.system.result.GetForegroundMediaTypeResult;
import cerence.ark.assistant.controller.domain.system.result.GetMediaStateBeforeConversationResult;
import cerence.ark.assistant.controller.domain.system.result.GetShortcutCommandsResult;
import cerence.ark.assistant.controller.domain.system.result.IsBluetoothHeadsetConnectedResult;
import cerence.ark.assistant.controller.enumeration.MediaTypeEnum;
import cerence.ark.assistant.controller.enumeration.ResultCodeEnum;
import cerence.ark.assistant.library.Utils;

public class DemoSystemDomain extends SystemDomain {
    public DemoSystemDomain(Dhi dhi) {
        super(dhi);
    }

    @Override
    protected void checkPermission(CheckPermissionParam param, IResponseSender<CheckPermissionResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(String.format(Utils.getContext().getString(R.string.system_check_permission),
                        param.getPermission()), false));
        // for test
        CheckPermissionResult result = new CheckPermissionResult();
        result.setGranted(true);
        responseSender.send(new Response<>(ResultCodeEnum.OK, result));
    }

    @Override
    protected void getMediaStateBeforeConversation(GetMediaStateBeforeConversationParam param,
            IResponseSender<GetMediaStateBeforeConversationResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(
                        String.format(Utils.getContext().getString(R.string.system_media_state), param.getMediaType()),
                        false));

        GetMediaStateBeforeConversationResult getMediaStateBeforeConversationResult = new GetMediaStateBeforeConversationResult();
        getMediaStateBeforeConversationResult.setMediaState(MusicController.getInstance(Utils.getContext()).getMediaState());
        responseSender.send(new Response<>(ResultCodeEnum.OK, getMediaStateBeforeConversationResult));
    }

    @Override
    protected void getActiveMediaTypeBeforeConversation(
            IResponseSender<GetActiveMediaTypeBeforeConversationResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(Utils.getContext().getString(R.string.system_active_media_type), false));
        GetActiveMediaTypeBeforeConversationResult result = new GetActiveMediaTypeBeforeConversationResult();
        result.setMediaType(MediaTypeEnum.NONE);
        responseSender.send(new Response<>(ResultCodeEnum.OK, result));
    }

    @Override
    protected void getForegroundMediaType(IResponseSender<GetForegroundMediaTypeResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(Utils.getContext().getString(R.string.system_foreground_media_type), false));
        GetForegroundMediaTypeResult result = new GetForegroundMediaTypeResult();
        result.setMediaType(MediaTypeEnum.NONE);
        responseSender.send(new Response<>(ResultCodeEnum.OK, result));
    }

    @Override
    protected void getShortcutCommands(IResponseSender<GetShortcutCommandsResult> responseSender) {
        GetShortcutCommandsResult shortcutCommandsResult = new GetShortcutCommandsResult();
        responseSender.send(new Response<>(ResultCodeEnum.OK, shortcutCommandsResult));
    }

    @Override
    protected void isBluetoothHeadsetConnected(IResponseSender<IsBluetoothHeadsetConnectedResult> iResponseSender) {

    }

    @Override
    public void activate() {
    }

    @Override
    public void deactivate() {
    }
}
