package com.mobiledrivetech.voiceassistant.customizedimpl;

import com.mobiledrivetech.voiceassistant.eventbus.MessageInfo;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

import cerence.ark.assistant.api.BaseResult;
import cerence.ark.assistant.api.Dhi;
import cerence.ark.assistant.api.Response;
import cerence.ark.assistant.api.controller.Domain;
import cerence.ark.assistant.api.controller.RequestHandler;
import cerence.ark.assistant.controller.enumeration.ResultCodeEnum;

/**
 * Defines a domain that receive all request messages.
 */
public class SimpleAllDomain extends Domain {
    public SimpleAllDomain(Dhi dhi) {
        super(dhi);
    }

    @Override
    public void loadRequestHandlers(Map<String, RequestHandler<? extends BaseResult>> map) {
        map.put("^[a-zA-Z_]+$", (request, responseSender) -> {
            MessageInfo messageEvent = new MessageInfo();
            messageEvent.setType("REQUEST");
            messageEvent.setJson(request.getParams());
            EventBus.getDefault().post(messageEvent);
            responseSender.send(new Response<>(ResultCodeEnum.OK));
        });
    }

    @Override
    public void activate() {
    }

    @Override
    public void deactivate() {
    }
}
