package com.mobiledrivetech.voiceassistant.customizedimpl;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.mobiledrivetech.voiceassistant.R;
import com.mobiledrivetech.voiceassistant.constants.VoiceConstants;
import com.mobiledrivetech.voiceassistant.eventbus.FinalOperation;
import com.mobiledrivetech.voiceassistant.music.MusicController;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;

import cerence.ark.assistant.api.BaseResult;
import cerence.ark.assistant.api.Dhi;
import cerence.ark.assistant.api.IResponseSender;
import cerence.ark.assistant.api.Response;
import cerence.ark.assistant.controller.domain.music.AbsMusicDomain;
import cerence.ark.assistant.controller.domain.music.param.CheckMusicAvailabilityParam;
import cerence.ark.assistant.controller.domain.music.param.CheckPlayListAvailabilityParam;
import cerence.ark.assistant.controller.domain.music.param.CheckSourceAvailabilityParam;
import cerence.ark.assistant.controller.domain.music.param.FavoriteControlParam;
import cerence.ark.assistant.controller.domain.music.param.GetSimilarArtistParam;
import cerence.ark.assistant.controller.domain.music.param.PlayControlParam;
import cerence.ark.assistant.controller.domain.music.param.PlayFavoriteMusicParam;
import cerence.ark.assistant.controller.domain.music.param.PlayMusicParam;
import cerence.ark.assistant.controller.domain.music.param.QueryArtistByAlbumParam;
import cerence.ark.assistant.controller.domain.music.param.QueryMusicInfoByTitleParam;
import cerence.ark.assistant.controller.domain.music.param.SetPlayModeParam;
import cerence.ark.assistant.controller.domain.music.result.CheckMusicAvailabilityResult;
import cerence.ark.assistant.controller.domain.music.result.CheckPlayListAvailabilityResult;
import cerence.ark.assistant.controller.domain.music.result.CheckSourceAvailabilityResult;
import cerence.ark.assistant.controller.domain.music.result.GetRecentPlayedSourceResult;
import cerence.ark.assistant.controller.domain.music.result.GetSimilarArtistResult;
import cerence.ark.assistant.controller.domain.music.result.IsFavoriteListFullResult;
import cerence.ark.assistant.controller.domain.music.result.IsMusicInfoVisibleResult;
import cerence.ark.assistant.controller.domain.music.result.QueryArtistByAlbumResult;
import cerence.ark.assistant.controller.domain.music.result.QueryCurrentMusicInfoResult;
import cerence.ark.assistant.controller.domain.music.result.QueryMusicInfoByTitleResult;
import cerence.ark.assistant.controller.enumeration.MediaSourceEnum;
import cerence.ark.assistant.controller.enumeration.MediaStateEnum;
import cerence.ark.assistant.controller.enumeration.ResultCodeEnum;
import cerence.ark.assistant.library.Utils;


/**
 * Custom implementation of Music Domain.
 * <p>
 * custom music domain extends {@link AbsMusicDomain}, because the music domain does not has default implementation.
 * Expect to Show:
 * 1. How to implement music domain.
 */
public class CustomMusicDomain extends AbsMusicDomain {
    private static final String TAG = CustomMusicDomain.class.getSimpleName();

    public CustomMusicDomain(Dhi dhi) {
        super(dhi);
    }

    @Override
    protected void queryCurrentMusicInfo(IResponseSender<QueryCurrentMusicInfoResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(Utils.getContext().getString(R.string.music_query_current_music_info), false));
        QueryCurrentMusicInfoResult musicInfoResult = new QueryCurrentMusicInfoResult();
        // TODO set the current music information
        // musicInfoResult.setMusicInfo();
        musicInfoResult.setSource(MediaSourceEnum.LOCAL); // Select the appropriate media source as required
        responseSender.send(new Response<>(ResultCodeEnum.OK, musicInfoResult));
    }

    @Override
    protected void queryMusicInfoByTitle(QueryMusicInfoByTitleParam param,
            IResponseSender<QueryMusicInfoByTitleResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(
                        String.format(Utils.getContext().getString(R.string.music_info_by_title), param.getTitle()),
                        false));
        QueryMusicInfoByTitleResult infoByTitleResult = new QueryMusicInfoByTitleResult();
        // TODO query the music info by title and set it into result.
        // infoByTitleResult.setMusicInfo();
        responseSender.send(new Response<>(ResultCodeEnum.OK, infoByTitleResult));
    }

    @Override
    protected void queryArtistByAlbum(QueryArtistByAlbumParam param,
            IResponseSender<QueryArtistByAlbumResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(
                        String.format(Utils.getContext().getString(R.string.music_artist_by_album), param.getAlbum()),
                        false));
        QueryArtistByAlbumResult artistByAlbumResult = new QueryArtistByAlbumResult();
        // TODO query the artist by album and set it into result.
        // artistByAlbumResult.setArtist(xx);
        responseSender.send(new Response<>(ResultCodeEnum.OK, artistByAlbumResult));
    }

    @Override
    protected void getSimilarArtist(GetSimilarArtistParam param,
            IResponseSender<GetSimilarArtistResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(
                        String.format(Utils.getContext().getString(R.string.music_similar_artist_by_artist),
                                param.getArtist()), false));
        GetSimilarArtistResult artistResult = new GetSimilarArtistResult();
        // TODO get the similar artist by artist and set it into result.
        // artistResult.setArtist(xx);
        responseSender.send(new Response<>(ResultCodeEnum.OK, artistResult));
    }

    @Override
    protected void checkMusicAvailability(CheckMusicAvailabilityParam param,
            IResponseSender<CheckMusicAvailabilityResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(
                        String.format(Utils.getContext().getString(R.string.music_check_music_availability),
                                param.getSource()), false));
        // for test
        CheckMusicAvailabilityResult result = new CheckMusicAvailabilityResult();
        result.setIsAvailable(true);
        responseSender.send(new Response<>(ResultCodeEnum.OK, result));
    }

    @Override
    protected void checkSourceAvailability(CheckSourceAvailabilityParam param,
            IResponseSender<CheckSourceAvailabilityResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(
                        String.format(Utils.getContext().getString(R.string.music_check_media_source_availability),
                                param.getSource()), false));
        // for test
        CheckSourceAvailabilityResult availabilityResult = new CheckSourceAvailabilityResult();
        availabilityResult.setIsAvailable(true);
        responseSender.send(new Response<>(ResultCodeEnum.OK, availabilityResult));
    }

    @Override
    protected void checkPlayListAvailability(CheckPlayListAvailabilityParam checkPlayListAvailabilityParam, IResponseSender<CheckPlayListAvailabilityResult> iResponseSender) {

    }

    @Override
    protected void playMusic(PlayMusicParam param, IResponseSender<BaseResult> responseSender) {
        MusicController musicController = MusicController.getInstance(Utils.getContext());
        try {
            EventBus.getDefault()
                    .post(new FinalOperation(
                            String.format(Utils.getContext().getString(R.string.music_play), param.toJsonString()),
                            false));


            musicController.parseMusicData(param.getMusicInfo().toJsonString());
            musicController.setMediaState(MediaStateEnum.PLAYING);
        } catch (JSONException e) {
            musicController.setMediaState(MediaStateEnum.UNKNOWN);
            e.printStackTrace();
        }
        responseSender.send(new Response<>(ResultCodeEnum.OK));
    }

    @Override
    protected void playFavoriteMusic(PlayFavoriteMusicParam param, IResponseSender<BaseResult> responseSender) {
        try {
            EventBus.getDefault()
                    .post(new FinalOperation(String.format(Utils.getContext().getString(R.string.music_favorite_play),
                            param.toJsonString()), false));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        responseSender.send(new Response<>(ResultCodeEnum.OK));
    }

    @Override
    protected void setPlayMode(SetPlayModeParam param, IResponseSender<BaseResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(
                        String.format(Utils.getContext().getString(R.string.music_set_play_mode), param.getPlayMode()),
                        false));
        responseSender.send(new Response<>(ResultCodeEnum.OK));
    }

    @Override
    protected void playControl(PlayControlParam param, IResponseSender<BaseResult> responseSender) {
        MusicController musicController = MusicController.getInstance(Utils.getContext());
        EventBus.getDefault()
                .post(new FinalOperation(String.format(Utils.getContext().getString(R.string.music_play_control),
                        param.getControlType()), false));

        switch (param.getControlType()) {
            case VoiceConstants.Music.MediaPlayControlType_NEXT:
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    public void run() {
                        Log.d(TAG, "really MediaPlayControlType_NEXT");
                        musicController.audioController(VoiceConstants.Music.STATUS_NEXT, 1);
                        musicController.setMediaState(MediaStateEnum.PLAYING);
                    }
                }, 2000);
                break;
            case VoiceConstants.Music.MediaPlayControlType_PREV:
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    public void run() {
                        Log.d(TAG, "really MediaPlayControlType_PREV");
                        musicController.audioController(VoiceConstants.Music.STATUS_PRE, 1);
                        musicController.setMediaState(MediaStateEnum.PLAYING);
                    }
                }, 2000);
                break;
            case VoiceConstants.Music.MediaPlayControlType_PAUSE:
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "really MediaPlayControlType_PAUSE");
                        musicController.audioController(VoiceConstants.Music.STATUS_PAUSE, 1);
                        musicController.setMediaState(MediaStateEnum.PAUSED);
                    }
                }, 2000);
                break;
            case VoiceConstants.Music.MediaPlayControlType_STOP:
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "really MediaPlayControlType_PAUSE");
                        musicController.audioController(VoiceConstants.Music.STATUS_PAUSE, 1);
                        musicController.setMediaState(MediaStateEnum.STOPPED);
                    }
                }, 2000);
                break;
            case VoiceConstants.Music.MediaPlayControlType_RESUME:
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    public void run() {
                        Log.d(TAG, "really MediaPlayControlType_RESUME");
                        musicController.audioController(VoiceConstants.Music.STATUS_RESUME, 1);
                        musicController.setMediaState(MediaStateEnum.PLAYING);
                    }
                }, 2000);
                break;
        }

        responseSender.send(new Response<>(ResultCodeEnum.OK));
    }

    @Override
    protected void favoriteControl(FavoriteControlParam param, IResponseSender<BaseResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(String.format(Utils.getContext().getString(R.string.music_favorite_control),
                        param.getControlType()), false));
        responseSender.send(new Response<>(ResultCodeEnum.OK));
    }

    @Override
    protected void isFavoriteListFull(IResponseSender<IsFavoriteListFullResult> responseSender) {
        // fot test
        IsFavoriteListFullResult isFavoriteListFullResult = new IsFavoriteListFullResult();
        isFavoriteListFullResult.setIsFull(false);
        responseSender.send(new Response<>(ResultCodeEnum.OK, isFavoriteListFullResult));
    }

    @Override
    protected void isMusicInfoVisible(IResponseSender<IsMusicInfoVisibleResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(Utils.getContext().getString(R.string.music_check_visible), false));
        // fot test
        IsMusicInfoVisibleResult isMusicInfoVisibleResult = new IsMusicInfoVisibleResult();
        isMusicInfoVisibleResult.setIsVisible(false);
        responseSender.send(new Response<>(ResultCodeEnum.OK, isMusicInfoVisibleResult));
    }

    @Override
    protected void getRecentPlayedSource(IResponseSender<GetRecentPlayedSourceResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(Utils.getContext().getString(R.string.music_played_source), false));
        // fot test
        GetRecentPlayedSourceResult result = new GetRecentPlayedSourceResult();
        result.setSource(MediaSourceEnum.LOCAL); // Select the appropriate media source as required
        responseSender.send(new Response<>(ResultCodeEnum.OK, result));
    }

    @Override
    protected void openMusicPlayer(IResponseSender<BaseResult> responseSender) {
        EventBus.getDefault().post(new FinalOperation(Utils.getContext().getString(R.string.music_open_player), false));
        responseSender.send(new Response<>(ResultCodeEnum.OK));
    }

    @Override
    public void activate() {
    }

    @Override
    public void deactivate() {
    }
}
