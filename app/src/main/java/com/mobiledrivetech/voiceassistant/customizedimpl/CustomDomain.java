package com.mobiledrivetech.voiceassistant.customizedimpl;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import cerence.ark.assistant.api.BaseResult;
import cerence.ark.assistant.api.Dhi;
import cerence.ark.assistant.api.IRequest;
import cerence.ark.assistant.api.IResponse;
import cerence.ark.assistant.api.IResponseSender;
import cerence.ark.assistant.api.Response;
import cerence.ark.assistant.api.controller.Domain;
import cerence.ark.assistant.api.controller.RequestHandler;
import cerence.ark.assistant.controller.enumeration.ResultCodeEnum;

/**
 * Defines a custom domain sample.
 * <p>
 * Expect to show:
 * 1. implements a custom domain that extents {@link Domain}
 * 2. implements a custom request handler {@link RequestHandler}.
 * 3. use {@link JSONObject} to parse parameters.
 * 4. use {@link IResponseSender} to send result data via {@link IResponseSender#send(IResponse)}
 */
public class CustomDomain extends Domain {
    public CustomDomain(Dhi dhi) {
        super(dhi);
    }

    @Override
    public void loadRequestHandlers(Map<String, RequestHandler<? extends BaseResult>> map) {
        map.put("request_name", new RequestHandler<BaseResult>() {
            @Override
            public void onHandle(IRequest request, IResponseSender<BaseResult> responseSender) {
                try {
                    // parse parameter
                    JSONObject jsonObject = new JSONObject(request.getParams());
                    String value1 = jsonObject.optString("param_name1");
                    String value2 = jsonObject.optString("param_name2");
                    int number = jsonObject.optInt("param_name3");
                    takeAction(value1);
                    // generate a result data and send to sdk
                    JSONObject resp = new JSONObject();
                    resp.putOpt("result_param_name1", "xx");
                    resp.putOpt("result_param_name2", 123);
                    resp.putOpt("result_param_name3", true);
                    // send json string
                    BaseResult baseResult = new BaseResult(resp.toString());
                    responseSender.send(new Response<>(ResultCodeEnum.OK, baseResult));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public boolean onAbort(IRequest request) {
                // TODO to abort this request action
                return false;
            }
        });
    }

    private void takeAction(String value) {
        // TODO use the value to do something.
    }

    @Override
    public void activate() {
    }

    @Override
    public void deactivate() {
    }
}
