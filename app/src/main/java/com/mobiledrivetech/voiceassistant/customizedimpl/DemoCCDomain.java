package com.mobiledrivetech.voiceassistant.customizedimpl;

import android.bluetooth.BluetoothAdapter;
import android.car.Car;
import android.car.media.CarAudioManager;
import android.content.Context;
import android.media.AudioManager;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.util.Log;

import com.mobiledrivetech.voiceassistant.R;
import com.mobiledrivetech.voiceassistant.VoiceAssistantService;
import com.mobiledrivetech.voiceassistant.eventbus.FinalOperation;
import com.mobiledrivetech.voiceassistant.hmi.picklist.PickListManager;

import org.greenrobot.eventbus.EventBus;

import cerence.ark.assistant.api.BaseResult;
import cerence.ark.assistant.api.Dhi;
import cerence.ark.assistant.api.IResponseSender;
import cerence.ark.assistant.api.Response;
import cerence.ark.assistant.controller.domain.cc.AbsCCDomain;
import cerence.ark.assistant.controller.domain.cc.param.CheckFunctionAvailabilityParam;
import cerence.ark.assistant.controller.domain.cc.param.CheckVolumeControlAvailabilityParam;
import cerence.ark.assistant.controller.domain.cc.param.CloseAppParam;
import cerence.ark.assistant.controller.domain.cc.param.ControlBluetoothParam;
import cerence.ark.assistant.controller.domain.cc.param.ControlVolumeParam;
import cerence.ark.assistant.controller.domain.cc.param.ControlWifiParam;
import cerence.ark.assistant.controller.domain.cc.param.GetVolumeParam;
import cerence.ark.assistant.controller.domain.cc.param.IsForegroundAppParam;
import cerence.ark.assistant.controller.domain.cc.param.OpenAppParam;
import cerence.ark.assistant.controller.domain.cc.param.SetScreenBrightnessParam;
import cerence.ark.assistant.controller.domain.cc.param.SetScreenStateParam;
import cerence.ark.assistant.controller.domain.cc.param.SetVolumeParam;
import cerence.ark.assistant.controller.domain.cc.result.CheckFunctionAvailabilityResult;
import cerence.ark.assistant.controller.domain.cc.result.CheckVolumeControlAvailabilityResult;
import cerence.ark.assistant.controller.domain.cc.result.GetActiveStreamTypeResult;
import cerence.ark.assistant.controller.domain.cc.result.GetScreenBrightnessResult;
import cerence.ark.assistant.controller.domain.cc.result.GetScreenStateResult;
import cerence.ark.assistant.controller.domain.cc.result.GetVolumeResult;
import cerence.ark.assistant.controller.domain.cc.result.IsForegroundAppResult;
import cerence.ark.assistant.controller.enumeration.CCFunctionEnum;
import cerence.ark.assistant.controller.enumeration.FunctionAvailabilityEnum;
import cerence.ark.assistant.controller.enumeration.ResultCodeEnum;
import cerence.ark.assistant.controller.enumeration.StreamTypeEnum;
import cerence.ark.assistant.controller.enumeration.SwitchEnum;
import cerence.ark.assistant.library.Utils;


public class DemoCCDomain extends AbsCCDomain {
    String TAG = this.getClass().getSimpleName();

    private AudioManager mAudioManager;
    private CarAudioManager mCarAudioManager;
    public DemoCCDomain(Dhi dhi) {
        super(dhi);
        mCarAudioManager = (CarAudioManager) VoiceAssistantService.getConnectedCar().getCarManager(Car.AUDIO_SERVICE);;
    }


    @Override
    protected void setVolume(SetVolumeParam param, IResponseSender<BaseResult> responseSender) {
//        int streamType = AudioManager.STREAM_MUSIC;
//        if (param.getPercentage() >= 0 && param.getPercentage() <= 100) {
//            switch (param.getStreamType()) {
//                case StreamTypeEnum.MUSIC:
//                    streamType = AudioManager.STREAM_MUSIC;
//                    break;
//                case StreamTypeEnum.NAVIGATION:
//                    streamType = AudioManager.STREAM_NOTIFICATION;
//                    break;
//                case StreamTypeEnum.SYSTEM:
//                    streamType = AudioManager.STREAM_SYSTEM;
//                    break;
//                case StreamTypeEnum.VOICE_CALL:
//                    streamType = AudioManager.STREAM_VOICE_CALL;
//                    break;
//                default:
//                    break;
//            }
//        }
//        mAudioManager.setStreamVolume(streamType,
//                mAudioManager.getStreamMaxVolume(streamType) * param.getPercentage() / 100,
//                AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);

        int volume = mCarAudioManager.getGroupMaxVolume(0) * param.getPercentage() / 100;

        Log.d(TAG, "setVolume: volume="+volume+", param.getPercentage()="+param.getPercentage());
        mCarAudioManager.setGroupVolume( CarAudioManager.PRIMARY_AUDIO_ZONE, 0, volume, AudioManager.FLAG_SHOW_UI | AudioManager.FLAG_PLAY_SOUND);


        responseSender.send(new Response<>(ResultCodeEnum.OK));
    }

    @Override
    protected void getVolume(GetVolumeParam param, IResponseSender<GetVolumeResult> responseSender) {
        GetVolumeResult getVolumeResult = new GetVolumeResult();
//        int streamType = AudioManager.STREAM_MUSIC;
//        switch (param.getStreamType()) {
//            case StreamTypeEnum.MUSIC:
//                streamType = AudioManager.STREAM_MUSIC;
//                break;
//            case StreamTypeEnum.NAVIGATION:
//                streamType = AudioManager.STREAM_NOTIFICATION;
//                break;
//            case StreamTypeEnum.SYSTEM:
//                streamType = AudioManager.STREAM_SYSTEM;
//                break;
//            case StreamTypeEnum.VOICE_CALL:
//                streamType = AudioManager.STREAM_VOICE_CALL;
//                break;
//            default:
//                break;
//        }
//        int volume = mAudioManager.getStreamVolume(streamType) * 100 / mAudioManager.getStreamMaxVolume(streamType);

        int volume = mCarAudioManager.getGroupVolume(CarAudioManager.PRIMARY_AUDIO_ZONE, 0) * 100/ mCarAudioManager.getGroupMaxVolume(0);

        Log.d(TAG, "getVolume: volume="+volume);

        getVolumeResult.setPercentage(volume);
        responseSender.send(new Response<>(ResultCodeEnum.OK, getVolumeResult));
    }

    @Override
    protected void controlVolume(ControlVolumeParam param, IResponseSender<BaseResult> responseSender) {
        volumeBeforeMute = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        switch (param.getAction()) {
            case SwitchEnum.OFF:
                if (volumeBeforeMute == 0) {
                    responseSender.send(new Response<>(ResultCodeEnum.ALREADY_EXPECTED));
                } else {
                    mediaMute();
                    responseSender.send(new Response<>(ResultCodeEnum.OK));
                }
                break;
            case SwitchEnum.ON:
                if (volumeBeforeMute != 0) {
                    responseSender.send(new Response<>(ResultCodeEnum.ALREADY_EXPECTED));
                } else {
                    mediaUnMute();
                    responseSender.send(new Response<>(ResultCodeEnum.OK));
                }
                break;
        }
    }

    @Override
    protected void setScreenBrightness(SetScreenBrightnessParam param, IResponseSender<BaseResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(String.format(Utils.getContext().getString(R.string.cc_set_screen_brightness),
                        String.valueOf(param.getPercentage())), false));
        responseSender.send(new Response<>(ResultCodeEnum.OK));
    }

    @Override
    protected void getScreenBrightness(IResponseSender<GetScreenBrightnessResult> responseSender) {
        GetScreenBrightnessResult result = new GetScreenBrightnessResult();
        try {
            result.setPercentage((int)(Settings.System.getInt(Utils.getContext().getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS_MODE) * 100 / 255));
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        responseSender.send(new Response<>(ResultCodeEnum.OK));
    }

    @Override
    protected void controlWifi(ControlWifiParam param, IResponseSender<BaseResult> responseSender) {
        WifiManager wifiManager = (WifiManager)Utils.getContext()
                                                    .getApplicationContext()
                                                    .getSystemService(Context.WIFI_SERVICE);
        switch (param.getAction()) {
            case SwitchEnum.ON:
                if (wifiManager.isWifiEnabled()) {
                    responseSender.send(new Response<>(ResultCodeEnum.ALREADY_EXPECTED));
                } else {
                    wifiManager.setWifiEnabled(true);
                    responseSender.send(new Response<>(ResultCodeEnum.OK));
                }
                break;
            case SwitchEnum.OFF:
                if (wifiManager.isWifiEnabled()) {
                    wifiManager.setWifiEnabled(false);
                    responseSender.send(new Response<>(ResultCodeEnum.OK));
                } else {
                    responseSender.send(new Response<>(ResultCodeEnum.ALREADY_EXPECTED));
                }
                break;
        }
    }

    @Override
    protected void controlBluetooth(ControlBluetoothParam param, IResponseSender<BaseResult> responseSender) {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        switch (param.getAction()) {
            case SwitchEnum.ON:
                if (bluetoothAdapter.isEnabled()) {
                    responseSender.send(new Response<>(ResultCodeEnum.ALREADY_EXPECTED));
                } else {
                    bluetoothAdapter.enable();
                    responseSender.send(new Response<>(ResultCodeEnum.OK));
                }
                break;
            case SwitchEnum.OFF:
                if (bluetoothAdapter.isEnabled()) {
                    bluetoothAdapter.disable();
                    responseSender.send(new Response<>(ResultCodeEnum.OK));
                } else {
                    responseSender.send(new Response<>(ResultCodeEnum.ALREADY_EXPECTED));
                }
                break;
        }
    }

    @Override
    protected void openApp(OpenAppParam param, IResponseSender<BaseResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(
                        String.format(Utils.getContext().getString(R.string.cc_open_app), param.getAppName()), false));
        responseSender.send(new Response<>(ResultCodeEnum.OK));
    }

    @Override
    protected void closeApp(CloseAppParam param, IResponseSender<BaseResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(
                        String.format(Utils.getContext().getString(R.string.cc_close_app), param.getAppName()), false));
        responseSender.send(new Response<>(ResultCodeEnum.OK));
    }

    @Override
    protected void cancel(IResponseSender<BaseResult> responseSender) {
        PickListManager.get().hidePickList();
        responseSender.send(new Response<>(ResultCodeEnum.OK));
    }

    @Override
    protected void checkFunctionAvailability(CheckFunctionAvailabilityParam param,
            IResponseSender<CheckFunctionAvailabilityResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(
                        String.format(Utils.getContext().getString(R.string.cc_check_function), param.getCcFunction()),
                        false));
        CheckFunctionAvailabilityResult result = new CheckFunctionAvailabilityResult();
        switch (param.getCcFunction()) {
            case CCFunctionEnum.WIFI:
            case CCFunctionEnum.SCREEN:
            case CCFunctionEnum.BT:
                result.setCheckResult(FunctionAvailabilityEnum.AVAILABLE);
                break;
            default:
                result.setCheckResult(FunctionAvailabilityEnum.UNAVAILABLE);
                break;
        }
        responseSender.send(new Response<>(ResultCodeEnum.OK, result));
    }

    @Override
    protected void setScreenState(SetScreenStateParam param, IResponseSender<BaseResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(
                        String.format(Utils.getContext().getString(R.string.cc_set_screen_state), param.getAction()),
                        false));
        responseSender.send(new Response<>(ResultCodeEnum.OK));
    }

    @Override
    protected void getScreenState(IResponseSender<GetScreenStateResult> responseSender) {
        GetScreenStateResult getScreenStateResult = new GetScreenStateResult();
        // fot test
        getScreenStateResult.setState(SwitchEnum.ON);
        responseSender.send(new Response<>(ResultCodeEnum.OK, getScreenStateResult));
    }

    @Override
    protected void getActiveStreamType(IResponseSender<GetActiveStreamTypeResult> responseSender) {
        GetActiveStreamTypeResult getActiveStreamTypeResult = new GetActiveStreamTypeResult();
        getActiveStreamTypeResult.setStreamType(StreamTypeEnum.DEFAULT);
        responseSender.send(new Response<>(ResultCodeEnum.OK, getActiveStreamTypeResult));
    }

    @Override
    protected void checkVolumeControlAvailability(CheckVolumeControlAvailabilityParam param,
            IResponseSender<CheckVolumeControlAvailabilityResult> responseSender) {
        // fot test
        CheckVolumeControlAvailabilityResult controlAvailabilityResult = new CheckVolumeControlAvailabilityResult();
        controlAvailabilityResult.setIsAvailable(true);
        responseSender.send(new Response<>(ResultCodeEnum.OK, controlAvailabilityResult));
    }

    @Override
    protected void isForegroundApp(IsForegroundAppParam param, IResponseSender<IsForegroundAppResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(
                        String.format(Utils.getContext().getString(R.string.cc_is_app_foreground), param.getAppName()),
                        false));
        // fot test
        IsForegroundAppResult result = new IsForegroundAppResult();
        result.setIsForeground(false);
        responseSender.send(new Response<>(ResultCodeEnum.OK, result));
    }

    @Override
    public void activate() {
        mAudioManager = (AudioManager)Utils.getContext().getSystemService(Context.AUDIO_SERVICE);
    }

    @Override
    public void deactivate() {
    }

    private int volumeBeforeMute = 0;

    private void mediaMute() {
        volumeBeforeMute = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, AudioManager.FLAG_PLAY_SOUND);
    }

    private void mediaUnMute() {
        if (volumeBeforeMute > 0) {
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volumeBeforeMute, AudioManager.FLAG_PLAY_SOUND);
        } else {
            int maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume / 2, AudioManager.FLAG_PLAY_SOUND);
        }
    }
}
