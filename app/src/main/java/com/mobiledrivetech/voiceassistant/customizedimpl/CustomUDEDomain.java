package com.mobiledrivetech.voiceassistant.customizedimpl;

import android.content.Context;
import android.util.Log;

import com.mobiledrivetech.voiceassistant.R;
import com.mobiledrivetech.voiceassistant.constants.VoiceConstants;
import com.mobiledrivetech.voiceassistant.controller.NavigationController;
import com.mobiledrivetech.voiceassistant.eventbus.FinalOperation;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import cerence.ark.assistant.api.BaseResult;
import cerence.ark.assistant.api.Dhi;
import cerence.ark.assistant.api.IResponseSender;
import cerence.ark.assistant.api.Response;
import cerence.ark.assistant.controller.domain.ude.AbsUDEDomain;
import cerence.ark.assistant.controller.domain.ude.param.ControlTrafficInfoViewParam;
import cerence.ark.assistant.controller.domain.ude.param.GetAddressByTypeParam;
import cerence.ark.assistant.controller.domain.ude.param.NavigateToDestinationParam;
import cerence.ark.assistant.controller.domain.ude.param.QueryTripInfoByLocationParam;
import cerence.ark.assistant.controller.domain.ude.param.SearchPoiByNameParam;
import cerence.ark.assistant.controller.domain.ude.param.SearchPoiCategoryByAddressParam;
import cerence.ark.assistant.controller.domain.ude.param.SetDefaultAddressByTypeParam;
import cerence.ark.assistant.controller.domain.ude.param.SetDefaultRouteOptionParam;
import cerence.ark.assistant.controller.domain.ude.param.SetMapDisplayModeParam;
import cerence.ark.assistant.controller.domain.ude.param.ZoomMapParam;
import cerence.ark.assistant.controller.domain.ude.result.GetAvoidPointsResult;
import cerence.ark.assistant.controller.domain.ude.result.GetMapDisplayModeResult;
import cerence.ark.assistant.controller.domain.ude.result.GetMaxWayPointsResult;
import cerence.ark.assistant.controller.domain.ude.result.GetRecentListResult;
import cerence.ark.assistant.controller.domain.ude.result.GetRouteOptionResult;
import cerence.ark.assistant.controller.domain.ude.result.GetWayPointsResult;
import cerence.ark.assistant.controller.domain.ude.result.IsMapOpenResult;
import cerence.ark.assistant.controller.domain.ude.result.IsNaviInfoVisibleResult;
import cerence.ark.assistant.controller.domain.ude.result.IsNavigatingResult;
import cerence.ark.assistant.controller.domain.ude.result.PlaceInfoResult;
import cerence.ark.assistant.controller.domain.ude.result.QueryTripInfoByLocationResult;
import cerence.ark.assistant.controller.domain.ude.result.SearchPoiByNameResult;
import cerence.ark.assistant.controller.domain.ude.result.SearchPoiCategoryByAddressResult;
import cerence.ark.assistant.controller.enumeration.MapDisplayModeEnum;
import cerence.ark.assistant.controller.enumeration.NaviRouteOptionEnum;
import cerence.ark.assistant.controller.enumeration.ResultCodeEnum;
import cerence.ark.assistant.controller.struct.PlaceInfo;
import cerence.ark.assistant.library.Utils;

/**
 * Custom implementation of Navi Domain.
 * <p>
 * custom navi domain extends {@link AbsUDEDomain}, because the navi domain does not has default implementation.
 * Expect to Show:
 * 1. How to implement navi domain.
 */
public class CustomUDEDomain extends AbsUDEDomain {
    private static final String TAG = CustomUDEDomain.class.getSimpleName();
    private Context mContext;

    public CustomUDEDomain(Dhi dhi) {
        super(dhi);
        mContext = Utils.getContext();
    }

    @Override
    protected void isMapOpen(IResponseSender<IsMapOpenResult> responseSender) {
        Log.d(TAG, "isMapOpen: ");
        IsMapOpenResult result = new IsMapOpenResult();
        result.setIsMapOpen(NavigationController.getInstance(mContext).isNaviTop());
        responseSender.send(new Response<>(ResultCodeEnum.OK, result));
    }

    @Override
    protected void openMap(IResponseSender responseSender) {
        Log.d(TAG, "openMap: ");
        EventBus.getDefault().post(new FinalOperation(mContext.getString(R.string.navi_open_map), false));
        NavigationController.getInstance(mContext).toNavigationAppView(mContext);
        testResponse(responseSender);
    }

    @Override
    protected void closeMap(IResponseSender responseSender) {
        Log.d(TAG, "closeMap: ");
        EventBus.getDefault().post(new FinalOperation(mContext.getString(R.string.navi_close_map), false));
        NavigationController.getInstance(mContext).closeMap();
        testResponse(responseSender);
    }

    @Override
    protected void isNavigating(IResponseSender<IsNavigatingResult> responseSender) {
        Log.d(TAG, "isNavigating: ");
        IsNavigatingResult result = new IsNavigatingResult();
        result.setIsNavigating(NavigationController.getInstance(mContext).isNaviStarted());
        responseSender.send(new Response<>(ResultCodeEnum.OK, result));
    }

    @Override
    protected void navigateToDestination(NavigateToDestinationParam param, IResponseSender responseSender) {
        Log.d(TAG, "navigateToDestination: ");
        try {
            EventBus.getDefault()
                    .post(new FinalOperation(String.format(mContext.getString(R.string.navi_start_navigation),
                            param.toJsonString()), false));
            PlaceInfo placeInfo = param.getDestination();
            Log.d(TAG, "navigateToDestination: " + placeInfo.toJsonString());
            NavigationController.getInstance(mContext).startCustomMapNavi(placeInfo.getName(), placeInfo.getAddress(), placeInfo.getLongitude(), placeInfo.getLatitude(), placeInfo.getDistance());
            testResponse(responseSender);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void exitNavigation(IResponseSender<BaseResult> responseSender) {
        Log.d(TAG, "exitNavigation: ");
        EventBus.getDefault()
                .post(new FinalOperation(mContext.getString(R.string.navi_exit_navigation), false));
        NavigationController.getInstance(mContext).stopNavRoute();
        testResponse(responseSender);
    }

    @Override
    protected void setDefaultRouteOption(SetDefaultRouteOptionParam param, IResponseSender<BaseResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(
                        String.format(mContext.getString(R.string.navi_set_default_route_option),
                                param.getRouteOption()), false));
        testResponse(responseSender);
    }

    @Override
    protected void getRouteOption(IResponseSender<GetRouteOptionResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(mContext.getString(R.string.navi_get_route_option), false));
        GetRouteOptionResult result = new GetRouteOptionResult();
        result.setRouteOption(NaviRouteOptionEnum.FASTEST);
        responseSender.send(new Response<>(ResultCodeEnum.OK, result));
    }

    @Override
    protected void getWayPoints(IResponseSender<GetWayPointsResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(mContext.getString(R.string.navi_get_route_option), false));
        GetWayPointsResult result = new GetWayPointsResult();
        result.setWayPointsList(new ArrayList<>());
        responseSender.send(new Response<>(ResultCodeEnum.OK, result));
    }

    @Override
    protected void getMaxWayPoints(IResponseSender<GetMaxWayPointsResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(mContext.getString(R.string.navi_max_way_point_count), false));
        GetMaxWayPointsResult getMaxWayPointsResult = new GetMaxWayPointsResult();
        getMaxWayPointsResult.setNumber(1);
        responseSender.send(new Response<>(ResultCodeEnum.OK, getMaxWayPointsResult));
    }

    @Override
    protected void getAvoidPoints(IResponseSender<GetAvoidPointsResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(mContext.getString(R.string.navi_avoid_point_list), false));
        // for test
        GetAvoidPointsResult getAvoidPointsResult = new GetAvoidPointsResult();
        getAvoidPointsResult.setAvoidPointsList(new ArrayList<>());
        responseSender.send(new Response<>(ResultCodeEnum.OK, getAvoidPointsResult));
    }

    @Override
    protected void searchPoiCategoryByAddress(SearchPoiCategoryByAddressParam param,
            IResponseSender<SearchPoiCategoryByAddressResult> responseSender) {
        try {
            EventBus.getDefault()
                    .post(new FinalOperation(
                            String.format(mContext.getString(R.string.navi_search_poi_by_address),
                                    param.toJsonString()), false));
            SearchPoiCategoryByAddressResult result = new SearchPoiCategoryByAddressResult();
            result.setPoiList(new ArrayList<>());
            responseSender.send(new Response<>(ResultCodeEnum.OK, result));
            //            testResponse(responseSender);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void getMapDisplayMode(IResponseSender<GetMapDisplayModeResult> responseSender) {
        EventBus.getDefault().post(new FinalOperation(mContext.getString(R.string.navi_display_mode), false));
        // fot test
        GetMapDisplayModeResult getMapDisplayModeResult = new GetMapDisplayModeResult();
        List<String> modeList = new ArrayList<>();
        modeList.add(MapDisplayModeEnum.MODE_2D); // Select the appropriate mode as required
        getMapDisplayModeResult.setModeList(modeList);
        responseSender.send(new Response<>(ResultCodeEnum.OK, getMapDisplayModeResult));
    }

    @Override
    protected void setMapDisplayMode(SetMapDisplayModeParam param, IResponseSender<BaseResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(String.format(mContext.getString(R.string.navi_set_map_display_mode),
                        param.getMode()), false));
        testResponse(responseSender);
    }

    @Override
    protected void zoomMap(ZoomMapParam param, IResponseSender<BaseResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(
                        String.format(mContext.getString(R.string.navi_zoom_map), param.getZoomType()),
                        false));
        switch (param.getZoomType()) {
            case "ZOOM_IN":
            case "ZOOM_IN_TO_MAX":
                NavigationController.getInstance(mContext).doNavigationOperate(VoiceConstants.NavigationType.NAVIGATION_MAP_BIGGER);
                break;
            case "ZOOM_OUT":
            case "ZOOM_OUT_TO_MIN":
                NavigationController.getInstance(mContext).doNavigationOperate(VoiceConstants.NavigationType.NAVIGATION_MAP_SMALLER);
                break;

        }

        testResponse(responseSender);
    }

    @Override
    protected void setDefaultAddressByType(SetDefaultAddressByTypeParam param,
            IResponseSender<BaseResult> responseSender) {
        try {
            EventBus.getDefault()
                    .post(new FinalOperation(
                            String.format(mContext.getString(R.string.navi_set_default_address),
                                    param.toJsonString()), false));
            testResponse(responseSender);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void getAddressByType(GetAddressByTypeParam param, IResponseSender<PlaceInfoResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(String.format(mContext.getString(R.string.navi_back_default_address),
                        param.getAddressType()), false));
        testResponse(responseSender);
    }

    @Override
    protected void queryTripInfoByLocation(QueryTripInfoByLocationParam param,
            IResponseSender<QueryTripInfoByLocationResult> responseSender) {
        try {
            EventBus.getDefault()
                    .post(new FinalOperation(
                            String.format(mContext.getString(R.string.navi_trip_info), param.toJsonString()),
                            false));
            testResponse(responseSender);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void searchPoiByName(SearchPoiByNameParam param, IResponseSender<SearchPoiByNameResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(String.format(mContext.getString(R.string.navi_search_poi_by_name),
                        param.getPoiName()), false));
        testResponse(responseSender);
    }

    @Override
    protected void getRecentList(IResponseSender<GetRecentListResult> responseSender) {
        EventBus.getDefault().post(new FinalOperation(mContext.getString(R.string.navi_get_recent), false));
        testResponse(responseSender);
    }

    @Override
    protected void controlTrafficInfoView(ControlTrafficInfoViewParam param,
            IResponseSender<BaseResult> responseSender) {
        EventBus.getDefault()
                .post(new FinalOperation(String.format(mContext.getString(R.string.navi_traffic_info_state),
                        param.getAction()), false));
        testResponse(responseSender);
    }

    @Override
    protected void isNaviInfoVisible(IResponseSender<IsNaviInfoVisibleResult> responseSender) {
        IsNaviInfoVisibleResult isNaviInfoVisibleResult = new IsNaviInfoVisibleResult();
        isNaviInfoVisibleResult.setIsVisible(NavigationController.getInstance(mContext).isNaviTop());
        responseSender.send(new Response<>(ResultCodeEnum.OK, isNaviInfoVisibleResult));
    }

    @Override
    public void activate() {
    }

    @Override
    public void deactivate() {
    }

    private void testResponse(IResponseSender responseSender) {
        responseSender.send(new Response<>(ResultCodeEnum.OK));
    }
}
