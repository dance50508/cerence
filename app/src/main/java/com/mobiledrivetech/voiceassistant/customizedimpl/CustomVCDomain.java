package com.mobiledrivetech.voiceassistant.customizedimpl;

import java.util.Map;

import cerence.ark.assistant.api.BaseResult;
import cerence.ark.assistant.api.Dhi;
import cerence.ark.assistant.api.IRequest;
import cerence.ark.assistant.api.IResponseSender;
import cerence.ark.assistant.api.Response;
import cerence.ark.assistant.api.controller.Domain;
import cerence.ark.assistant.api.controller.RequestHandler;
import cerence.ark.assistant.controller.domain.vehiclecontrol.result.QueryEnergyTypeResult;
import cerence.ark.assistant.controller.domain.vehiclecontrol.result.QueryRemainingMileageResult;
import cerence.ark.assistant.controller.enumeration.EnergyTypeEnum;
import cerence.ark.assistant.controller.enumeration.MileageUnitEnum;
import cerence.ark.assistant.controller.enumeration.ResultCodeEnum;

public class CustomVCDomain extends Domain {
    public CustomVCDomain(Dhi dhi) {
        super(dhi);
    }

    @Override
    public void loadRequestHandlers(Map<String, RequestHandler<? extends BaseResult>> map) {
        map.put("queryEnergyType", new RequestHandler<QueryEnergyTypeResult>() {
            @Override
            public void onHandle(IRequest request, IResponseSender<QueryEnergyTypeResult> responseSender) {
                QueryEnergyTypeResult result = new QueryEnergyTypeResult();
                result.setEnergyType(EnergyTypeEnum.ELECTRIC);
                responseSender.send(new Response<>(ResultCodeEnum.OK, result));
            }
        });

        map.put("queryRemainingMileage", new RequestHandler<QueryRemainingMileageResult>() {
            @Override
            public void onHandle(IRequest request, IResponseSender<QueryRemainingMileageResult> responseSender) {
                QueryRemainingMileageResult result = new QueryRemainingMileageResult();
                result.setRemainingMileage(800);
                result.setUnit(MileageUnitEnum.KM);
                responseSender.send(new Response<>(ResultCodeEnum.OK, result));
            }
        });
    }

    @Override
    public void activate() {
    }

    @Override
    public void deactivate() {
    }
}
