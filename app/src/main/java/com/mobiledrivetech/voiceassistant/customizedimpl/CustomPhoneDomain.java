package com.mobiledrivetech.voiceassistant.customizedimpl;

import android.Manifest;
import android.content.Context;
import android.os.Build;
import android.telecom.TelecomManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.blankj.utilcode.util.LogUtils;
import com.mobiledrivetech.voiceassistant.CarVoiceAssistantApplication;
import com.mobiledrivetech.voiceassistant.R;
import com.mobiledrivetech.voiceassistant.eventbus.FinalOperation;

import org.greenrobot.eventbus.EventBus;

import cerence.ark.assistant.api.ArkAssistant;
import cerence.ark.assistant.api.BaseResult;
import cerence.ark.assistant.api.Dhi;
import cerence.ark.assistant.api.IResponseSender;
import cerence.ark.assistant.api.Response;
import cerence.ark.assistant.common.Logger;
import cerence.ark.assistant.controller.domain.phone.PhoneDomain;
import cerence.ark.assistant.controller.domain.phone.param.CallOperationParam;
import cerence.ark.assistant.controller.domain.phone.param.MakeCallParam;
import cerence.ark.assistant.controller.domain.phone.result.MakeCallResult;
import cerence.ark.assistant.controller.enumeration.PhoneCallActionEnum;
import cerence.ark.assistant.controller.enumeration.ResultCodeEnum;
import cerence.ark.assistant.controller.event.CallEndedEvent;
import cerence.ark.assistant.controller.event.IncomingCallEvent;
import cerence.ark.assistant.controller.event.RejectIncomingCallEvent;
import cerence.ark.assistant.library.Utils;

import android.content.Intent;
import android.net.Uri;


/**
 * Defines phone domain that extends {@link PhoneDomain}
 * <p>
 * Expect to show:
 * override a method {@link CustomPhoneDomain#callOperation(CallOperationParam, IResponseSender)} that to be
 * re-implemented by yourself
 */
public class CustomPhoneDomain extends PhoneDomain {
    private static final String TAG = CustomPhoneDomain.class.getSimpleName();
    private boolean isRinging = false;
    private boolean isOffHook = false;
    private final PhoneStateListener mPhoneStateListener = new PhoneStateListener() {
        @Override
        public void onCallStateChanged(int state, String phoneNumber) {
            switch (state) {
                case TelephonyManager.CALL_STATE_IDLE:
                    LogUtils.dTag(TAG, "CALL_STATE_IDLE");
                    if (isRinging) {
                        if (isOffHook) {
                            ArkAssistant.get().sendAppEvent(new CallEndedEvent.Builder().build(), null);
                        } else {
                            ArkAssistant.get().sendAppEvent(new RejectIncomingCallEvent.Builder().build(), null);
                        }
                    }
                    isRinging = false;
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    LogUtils.dTag(TAG, "CALL_STATE_OFFHOOK");
                    isOffHook = true;
                    break;
                case TelephonyManager.CALL_STATE_RINGING:
                    LogUtils.dTag(TAG, "CALL_STATE_RINGING");
                    isRinging = true;
                    ArkAssistant.get().sendAppEvent(new IncomingCallEvent.Builder().build(), null);
                    break;
                default:
                    break;
            }
        }
    };

    public CustomPhoneDomain(Dhi dhi) {
        super(dhi);
    }

    @Override
    public void activate() {
        super.activate();
        TelephonyManager telephonyManager = (TelephonyManager) CarVoiceAssistantApplication.getInstance()
                                                                               .getSystemService(
                                                                                       Context.TELEPHONY_SERVICE);
        if (telephonyManager != null) {
            telephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
        }
    }

    @Override
    protected void callOperation(CallOperationParam param, IResponseSender<BaseResult> responseSender) {
        Response<BaseResult> response = new Response<>(ResultCodeEnum.OK);
        String text = "";
        switch (param.getActionType()) {
            case PhoneCallActionEnum.ANSWER_CALL:
                text = Utils.getContext().getString(R.string.phone_action_answer_call);
                EventBus.getDefault()
                        .post(new FinalOperation(text, true));
                if (!acceptCall()) {
                    response.setResultCode(ResultCodeEnum.ERROR);
                }
                break;
            case PhoneCallActionEnum.REJECT_INCOMING_CALL:
                if (!rejectCall()) {
                    response.setResultCode(ResultCodeEnum.ERROR);
                }
                text = Utils.getContext().getString(R.string.phone_action_reject_incoming_cal);
                EventBus.getDefault()
                        .post(new FinalOperation(text, true));
                break;
            case PhoneCallActionEnum.HANGUP_OUTGOING_CALL:
                text = Utils.getContext().getString(R.string.phone_action_hangup_outgoing_call);
                EventBus.getDefault()
                        .post(new FinalOperation(text, true));
                if (!rejectCall()) {
                    response.setResultCode(ResultCodeEnum.ERROR);
                }
                break;
            case PhoneCallActionEnum.IGNORE_INCOMING_CALL:
                text = Utils.getContext().getString(R.string.phone_action_ignore_incoming_call);
                EventBus.getDefault()
                        .post(new FinalOperation(text, true));
                break;
            case PhoneCallActionEnum.REPLY_CALL_BY_TEXT:
                text = Utils.getContext().getString(R.string.phone_action_reply_call_by_text);
                EventBus.getDefault()
                        .post(new FinalOperation(text, true));
                if (rejectCall()) {
                    response.setResultCode(ResultCodeEnum.OK);
                } else {
                    response.setResultCode(ResultCodeEnum.ERROR);
                }
                break;
            default:
                text = "error";
                response.setResultCode(ResultCodeEnum.ERROR);
                break;
        }
        Log.d(TAG, "callOperation: " + text);
        responseSender.send(response);
    }

    @Override
    protected void makeCall(MakeCallParam param, IResponseSender<MakeCallResult> responseSender) {
        MakeCallResult makeCallResult = new MakeCallResult();
        makeCallResult.setNeedUserConfirmation(false);
        responseSender.send(new Response<>(ResultCodeEnum.OK, makeCallResult));
        String phoneNumber = param.getPhoneNumber();
        EventBus.getDefault()
                .post(new FinalOperation(String.format(Utils.getContext().getString(R.string.phone_action_make_call),
                        phoneNumber), false));
        Log.d(TAG, "makeCall: " + Utils.getContext().getString(R.string.phone_action_make_call, phoneNumber));
        Intent intentPhone = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber));
        intentPhone.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Utils.getContext().startActivity(intentPhone);
    }

    private boolean acceptCall() {
        if (!Utils.checkSelfPermission(Manifest.permission.ANSWER_PHONE_CALLS)) {
            Log.e(TAG, "acceptCall: the android.permission.ANSWER_PHONE_CALLS was denied.");
            return false;
        }
        try {
            TelecomManager telecomManager = (TelecomManager)Utils.getContext()
                                                                 .getSystemService(Context.TELECOM_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                telecomManager.acceptRingingCall();
            }
            return true;
        } catch (Exception e) {
            Log.e(TAG, "acceptCall: exception", e);
        }
        return false;
    }

    private boolean rejectCall() {
        if (!Utils.checkSelfPermission(Manifest.permission.ANSWER_PHONE_CALLS)) {
            Logger.error(TAG, "rejectCall: the android.permission.ANSWER_PHONE_CALLS was denied.");
            return false;
        }
        try {
            TelecomManager telecomManager = (TelecomManager)Utils.getContext()
                                                                 .getSystemService(Context.TELECOM_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                return telecomManager.endCall();
            }
        } catch (Exception e) {
            Logger.error(TAG, "rejectCall: exception", e);
        }
        return false;
    }
}
