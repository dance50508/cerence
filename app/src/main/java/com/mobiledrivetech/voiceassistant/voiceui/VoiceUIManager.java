package com.mobiledrivetech.voiceassistant.voiceui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.mobiledrivetech.voiceassistant.CarVoiceAssistantApplication;
import com.mobiledrivetech.voiceassistant.R;
import com.mobiledrivetech.voiceassistant.WifiSettings;
import com.mobiledrivetech.voiceassistant.bean.BaseBean;
import com.mobiledrivetech.voiceassistant.bean.WeatherBean;
import com.mobiledrivetech.voiceassistant.constants.VoiceConstants;
import com.mobiledrivetech.voiceassistant.hmi.picklist.BaseSizeAdapter;
import com.mobiledrivetech.voiceassistant.hmi.picklist.IControlPickList;
import com.mobiledrivetech.voiceassistant.hmi.picklist.PickListManager;
import com.mobiledrivetech.voiceassistant.hmi.picklist.PickListView;
import com.mobiledrivetech.voiceassistant.view.MutedVideoView;

import org.jetbrains.annotations.NotNull;

import cerence.ark.assistant.api.ArkAssistant;
import cerence.ark.assistant.api.notification.DialogStateChanged;
import cerence.ark.assistant.controller.event.PickListItemSelectedEvent;
import skin.support.content.res.SkinCompatResources;

//import com.mobiledrivetech.voiceassistantalexa.bean.BaseBean;
//import com.mobiledrivetech.voiceassistantalexa.bean.NewsBean;
//import com.mobiledrivetech.voiceassistantalexa.bean.WeatherBean;
//import com.mobiledrivetech.voiceassistantalexa.constants.VoiceConstants;
//import com.mobiledrivetech.voiceassistantalexa.ui.AudioWaveLayout;
//import com.mobiledrivetech.voiceassistantalexa.utils.WifiSettings;


public class VoiceUIManager implements IControlPickList {

    private String TAG = VoiceUIManager.class.getSimpleName();

    private Context mContext;
    private static VoiceUIManager mInstance;
    private WindowManager mWindowManager;
    private WindowManager.LayoutParams wmParams;
    private FrameLayout mFloatLayout;
//    private AudioWaveLayout mAudioWaveLayout;
    private MutedVideoView mVideoView;
    private View mWaveBgView;
    private View mNetworkView;
    private FrameLayout mChatLayout;
    private View mWaveViewLayout;
    private PickListView mPickListView;
    private LayoutInflater mLayoutInflater;
    private TranslateAnimation mShowAction;
    private TranslateAnimation mHiddenAction;
    private Handler mMainHandler = new Handler(Looper.getMainLooper());
    private boolean mIsInSession = true;

    /**
     *
     */
    private VoiceUIManager() {

    }

    public static VoiceUIManager getInstance() {
        if (mInstance == null) {
            synchronized (VoiceUIManager.class) {
                if (mInstance == null) {
                    mInstance = new VoiceUIManager();
                }
            }
        }
        return mInstance;
    }

    public void init(Context context) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(mContext);
        createFloatView();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void createFloatView() {
        mMainHandler.post(() -> {
            wmParams = new WindowManager.LayoutParams();
            mWindowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                wmParams.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
            } else {
                wmParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_DIALOG;
            }
            wmParams.format = PixelFormat.RGBA_8888;
            wmParams.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE /*| WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE*/;
            wmParams.gravity = Gravity.START | Gravity.TOP;
            wmParams.x = 0;
            wmParams.y = 0;
            wmParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            wmParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

            mFloatLayout = (FrameLayout) mLayoutInflater.inflate(R.layout.fca_main_view, null);
            mPickListView = mFloatLayout.findViewById(R.id.lv_pick_list);
//            mAudioWaveLayout = mFloatLayout.findViewById(R.id.wave_view);
            mVideoView = mFloatLayout.findViewById(R.id.wave_view);
            mWaveBgView = mFloatLayout.findViewById(R.id.wave_bg);
            mWindowManager.addView(mFloatLayout, wmParams);

            mChatLayout = mFloatLayout.findViewById(R.id.chat_layout);
            mWaveViewLayout = mFloatLayout.findViewById(R.id.wave_view_layout);

            mFloatLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //hideFloatWindow();
                }
            });
            mNetworkView = mFloatLayout.findViewById(R.id.no_net_work);
            mNetworkView.findViewById(R.id.network_refresh).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            mNetworkView.findViewById(R.id.network_set).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hideFloatWindow();
                    WifiSettings.openSetting(mContext);
                }
            });
            initVideo();
        });
    }

    public void showFloatWindow() {
        mMainHandler.post(() -> {
            if (mWaveViewLayout != null) {
                if (mFloatLayout.getVisibility() == View.VISIBLE)
                    return;
                mFloatLayout.setVisibility(View.VISIBLE);
                if (mShowAction == null) {
                    mShowAction = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                            Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                            -1.0f, Animation.RELATIVE_TO_SELF, 0.0f);
                    mShowAction.setDuration(500);
                    mShowAction.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            Log.d(TAG, "showFloatWindow onAnimationStart");
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            Intent intent = new Intent();
                            intent.setAction("voice_assistant_status_changed");
                            intent.putExtra("activated", true);
                            mContext.sendBroadcast(intent);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    mShowAction.setFillAfter(true);
                }

                mWaveViewLayout.startAnimation(mShowAction);

            }
        });
    }

    public boolean isShowFloatWindow() {
        return mFloatLayout != null && mFloatLayout.getVisibility() == View.VISIBLE;

    }

    public void hideFloatWindow() {
        mMainHandler.post(() -> {
            if (mWaveViewLayout != null) {
                if (mFloatLayout.getVisibility() == View.GONE)
                    return;
                if (mHiddenAction == null) {
                    mHiddenAction = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                            Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                            0.0f, Animation.RELATIVE_TO_SELF, -1.0f);
                    mHiddenAction.setDuration(500);
                    mHiddenAction.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            //  mWindowManager.removeView(mFloatLayout);
                            mFloatLayout.setVisibility(View.GONE);
                            mNetworkView.setVisibility(View.GONE);
                            mChatLayout.removeAllViews();
                            Log.d(TAG, "hideFloatWindow onAnimationEnd");
                            Intent intent = new Intent();
                            intent.setAction("voice_assistant_status_changed");
                            intent.putExtra("activated", false);
                            mContext.sendBroadcast(intent);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    mHiddenAction.setFillAfter(true);
                }
                mWaveViewLayout.startAnimation(mHiddenAction);
            }
        });

    }

    public void updateUI(final BaseBean bean) {
        if (bean == null)
            return;
        Log.d(TAG, "updateUI viewType =" + bean.getType());
        mMainHandler.post(new Runnable() {
            @Override
            public void run() {
                mChatLayout.removeAllViews();
                View view = null;
                TextView tv = null;
                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                if (isInSession()) {
                    showFloatWindow();

                    switch (bean.getType()) {
                        case VoiceConstants.WidgetType.TYPE_INPUT:
                            view = mLayoutInflater.inflate(R.layout.listening_view, null);
                            tv = view.findViewById(R.id.listening_text_view);
                            tv.setText(bean.getUtterance());
//                        mAudioWaveLayout.toListen();
                            startInputVideo();
                            layoutParams.height = mContext.getResources().getDimensionPixelOffset(R.dimen.size_24_dp);
                            break;
                        case VoiceConstants.WidgetType.TYPE_OUTPUT:
//                        mAudioWaveLayout.toSpeak();
                            startOutputVideo();
                            view = mLayoutInflater.inflate(R.layout.listening_view, null);
                            tv = view.findViewById(R.id.listening_text_view);
                            tv.setText(bean.getUtterance());
                            layoutParams.height = mContext.getResources().getDimensionPixelOffset(R.dimen.size_24_dp);
                            break;
                        case VoiceConstants.WidgetType.TYPE_WIDGET_WEATHER:
                            WeatherBean weatherBean = (WeatherBean) bean;
//                        mAudioWaveLayout.toSpeak();
                            startOutputVideo();
                            hidePickList();
                            view = mLayoutInflater.inflate(R.layout.weather_view, null);
                            ImageView weatherIcon = view.findViewById(R.id.img_weather);
                            TextView txt_weather = view.findViewById(R.id.txt_weather);
                            TextView txt_area = view.findViewById(R.id.txt_area);
                            TextView txt_temperature = view.findViewById(R.id.txt_temperature);
                            txt_weather.setText(weatherBean.getWeather());
                            txt_area.setText(weatherBean.getCity());
                            String format = mContext.getResources().getString(R.string.weather_tem_format);
                            String result = String.format(format, weatherBean.getLowTemperature());
                            txt_temperature.setText(result);
                            //Glide.with(mContext).load(getImage(weatherBean.getCurrentWeatherIcon())).into(weatherIcon);
                            weatherIcon.setImageResource(getImage(weatherBean.getCurrentWeatherIcon()));

                            layoutParams.height = mContext.getResources().getDimensionPixelOffset(R.dimen.size_24_dp);

                            break;
                        default:
                            hideFloatWindow();
                            return;
                    }

                    mChatLayout.addView(view, layoutParams);
                }
            }
        });

    }

    private int getImage(String imageName) {
        int drawableResourceId = mContext.getResources().getIdentifier(imageName, "drawable", mContext.getPackageName());
        return drawableResourceId;
    }

    public void showNetWorkUI(boolean show) {
        mMainHandler.post(new Runnable() {
            @Override
            public void run() {
                if (show && mNetworkView.getVisibility() != View.VISIBLE) {
                    mNetworkView.setVisibility(View.VISIBLE);
                    mMainHandler.removeCallbacks(runnable);
                    mMainHandler.postDelayed(runnable, 8000);
                } else if (!show && mNetworkView.getVisibility() == View.VISIBLE) {
                    mNetworkView.setVisibility(View.GONE);
                    mMainHandler.removeCallbacks(runnable);
                }
            }
        });

    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            hideFloatWindow();
        }
    };

    public void dialogStateChanged(final DialogStateChanged.State state) {
        mMainHandler.post(() -> {
            Log.d(TAG, "dialogStateChanged:: " + state);
            switch (state) {
                case IDLE:
                case LISTENING_WUW:
                    hideFloatWindow();
//                    mAudioWaveLayout.toIdle();
                    stopVideo();
                    setIsInSession(true);
                    break;
//                case SPEAKING:
//                    mAudioWaveLayout.toSpeak();
//                    startOutputVideo();
//                    break;
                case LISTENING_CMD:
                    if (isInSession()) {
                        showFloatWindow();
//                    mAudioWaveLayout.toListen();
                        startInputVideo();
                    }
                    break;
                case PROCESSING:
                    startOutputVideo();
                    break;
                case CAPTURING://while sdk in analyzing what we've spoke
                    break;
                default:
                    break;

            }
        });

    }

    public void release() {
//        if (mAudioWaveLayout != null) {
//            mAudioWaveLayout.release();
//            mAudioWaveLayout = null;
//        }
        stopVideo();
    }

    @Override
    public void showPickList(BaseSizeAdapter adapter, @Nullable Integer customHeight) {
        Log.d(TAG, "showPickList");
        if (null != mPickListView) {
            adapter.setUpdateUiListener(new BaseSizeAdapter.UpdateUiListener() {
                @Override
                public void updateUi(int curPage, int totalPage) {
                    mPickListView.updatePageUi(curPage, totalPage);
                }
            });
            adapter.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(@NonNull @NotNull BaseQuickAdapter<?, ?> adapter, @NonNull @NotNull View view,
                                        int position) {
                    ArkAssistant.get()
                            .sendAppEvent(
                                    new PickListItemSelectedEvent.Builder().setItemOrdinal(position + 1).build(),
                                    null);
                    PickListManager.get().hidePickList();
                }
            });
            mPickListView.setAdapter(adapter, customHeight);
            mPickListView.setVisibility(View.VISIBLE);
            wmParams.height = WindowManager.LayoutParams.MATCH_PARENT;
            mWindowManager.updateViewLayout(mFloatLayout, wmParams);
        }
    }

    @Override
    public void hidePickList() {
        Log.d(TAG, "hidePickList");
        mMainHandler.post(new Runnable() {
            @Override
            public void run() {
                if (null != mPickListView) {
                    mPickListView.setAdapter(null, null);
                    mPickListView.setVisibility(View.GONE);
                }
                wmParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                mWindowManager.updateViewLayout(mFloatLayout, wmParams);
            }
        });
    }

    private void initVideo() {
        Log.d(TAG, "initVideo");

        //to make black view not so obviously before video start
        mVideoView.setOnPreparedListener(mediaPlayer -> {
            mWaveBgView.setVisibility(View.VISIBLE);
            mediaPlayer.setOnInfoListener((mediaPlayer1, i, i1) -> {
                if (i == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                    mWaveBgView.setVisibility(View.GONE);
                }
                mVideoView.setBackgroundColor(Color.TRANSPARENT);
                return true;
            });
            mediaPlayer.setLooping(true);
        });

        mVideoView.setOnErrorListener((mediaPlayer, i, i1) -> {
            Log.d(TAG, "videoView onError");
            mVideoView.setVisibility(View.GONE);
            return false;
        });
    }

    private void startInputVideo() {
        Log.d(TAG, "startInputVideo");

        if(!CarVoiceAssistantApplication.ThemeType.equals(CarVoiceAssistantApplication.THEME_TYPE_DEF)){
            startVideo(R.raw.voice_animation_input_day);
        } else {
            startVideo(R.raw.voice_animation_input);
        }

        Log.d(TAG, "###_"+SkinCompatResources.getInstance().getTargetResId(mContext,R.raw.voice_animation_input)+"__"+R.raw.voice_animation_input);
    }

    private void startOutputVideo() {
        Log.d(TAG, "startOutputVideo");

        if(!CarVoiceAssistantApplication.ThemeType.equals(CarVoiceAssistantApplication.THEME_TYPE_DEF)){
            startVideo(R.raw.voice_animation_output_day);
        } else {
            startVideo(R.raw.voice_animation_output);
        }

        Log.d(TAG, "###_"+SkinCompatResources.getInstance().getTargetResId(mContext,R.raw.voice_animation_output)+"__"+R.raw.voice_animation_output);
    }

    private void startVideo(int videoId) {
        stopVideo();
        String path = "android.resource://" + mContext.getPackageName() + "/" + videoId;
        mVideoView.setVideoURI(Uri.parse(path));
        mVideoView.start();
        mVideoView.setVisibility(View.VISIBLE);
    }

    private void stopVideo() {
        Log.d(TAG, "stopVideo");
        if (mVideoView == null) {
            Log.d(TAG, "stopVideo: videoView is null. return");
            return;
        }
        mVideoView.stopPlayback();
    }

    public boolean isInSession() {
        return mIsInSession;
    }

    public void setIsInSession(boolean mIsInSession) {
        this.mIsInSession = mIsInSession;
    }
}
