package com.mobiledrivetech.voiceassistant.voiceui;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.mobiledrivetech.voiceassistant.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class AudioWaveLayout extends RelativeLayout {
    private static final String TAG = "CarAlexa_"+ AudioWaveLayout.class.getSimpleName();
    private final static int cover = 3;
    private final static int STATE_IDLE = 0;
    private final static int STATE_LISTEN = 1;
    private final static int STATE_SPEAK = 2;
    private Context mContext;
    private int viewWidth;
    private int viewHeight;
    private int itemWidth;
    private int itemHeight;
    private int viewMarginLeft;
    private Bitmap mBitmapBlue;
    private Bitmap mBitmapPurple;
    private Bitmap mBitmapRed;
    private Random random;
    private int mStatus;
    private boolean mThemeStyle;

    //旋转木马旋转半径  圆的半径
    private float mCarouselR;
    private float mOriginalR;

    //旋转的角度
    private float mAngle = 0;

    //旋转木马子view
    private List<View> mCarouselViews = new ArrayList<>();
    private List<ObjectAnimator> animatorList;

    //旋转木马子view的数量
    private int viewCount;

    //半径扩散收缩动画
    private ValueAnimator mAnimationR;
    //旋转动画
    private ValueAnimator restAnimator;
    private int[] mImageRes = {R.drawable.listening_05, R.drawable.listening_02, R.drawable.listening_01,
            R.drawable.listening_03, R.drawable.listening_05, R.drawable.listening_04,
            R.drawable.listening_02, R.drawable.listening_01, R.drawable.listening_03,};

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            invalidate();
        }
    };

    public AudioWaveLayout(Context context) {
        super(context);
        init(context, null);
    }

    public AudioWaveLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        viewWidth = MeasureSpec.getSize(widthMeasureSpec);
        viewHeight = MeasureSpec.getSize(heightMeasureSpec);
        if (mBitmapBlue != null && mBitmapPurple != null && mBitmapRed != null) {
            viewMarginLeft = viewWidth / 2 - mBitmapBlue.getWidth() - mBitmapPurple.getWidth() - mBitmapRed.getWidth() / 2 + 2 * cover;
        }
        Log.i(TAG, "width = " + viewWidth + " , height = " + viewHeight + " -- " + viewMarginLeft);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        switch (mStatus) {
            case STATE_IDLE:
                startDrawIdle(canvas);
                break;
            case STATE_LISTEN:
                startDrawRandom(canvas);
                handler.sendEmptyMessageDelayed(0, 100);
                break;
            case STATE_SPEAK:
               /* startDrawUp(canvas);
                setWillNotDraw(true);*/
                break;
        }
    }

    private void init(Context context, AttributeSet attrs) {
        random = new Random();
        mContext = context;
        mCarouselR = 540;
        mThemeStyle = false;
        initBitmap(context);
        mOriginalR = mCarouselR;
    }

    private void initBitmap(Context context) {
        mBitmapBlue = BitmapFactory.decodeResource(context.getResources(), R.drawable.voice_talking1);
        mBitmapPurple = BitmapFactory.decodeResource(context.getResources(), R.drawable.voice_talking2);
        mBitmapRed = BitmapFactory.decodeResource(context.getResources(), R.drawable.voice_talking3);
        itemWidth = mBitmapBlue.getWidth();
        itemHeight = mBitmapBlue.getHeight();
        Log.i(TAG, "initBitmap mThemeStyle " + mThemeStyle + "red_width = " + mBitmapBlue.getWidth() + " , height = " + mBitmapBlue.getHeight() + " -- " + viewMarginLeft);
    }

    private void startDrawRandom(Canvas canvas) {
        drawSingle(canvas, mBitmapBlue, 0, 0);
        drawSingle(canvas, mBitmapPurple, 1, cover);
        drawSingle(canvas, mBitmapRed, 2, cover);
        drawSingle(canvas, mBitmapPurple, 3, cover);
        drawSingle(canvas, mBitmapBlue, 4, cover);
    }

    private void drawSingle(Canvas canvas, Bitmap bitmap, int position, int cover) {
        int temp = random.nextInt(viewHeight);
        int randomHeight = Math.max(temp, itemHeight);
        drawVerticalLine(canvas, bitmap, (itemWidth - cover) * position, randomHeight);
    }

    private void drawVerticalLine(Canvas canvas, Bitmap bitmap, int left, int totalHeight) {
        int height = bitmap.getHeight();
        for (int i = 1; i * height <= totalHeight; i++) {
            canvas.drawBitmap(bitmap, viewMarginLeft + left, viewHeight - i * height, null);
        }
    }

   /* private void startDrawUp(Canvas canvas) {
        Log.i(TAG, "draw up");
       // for (int current = itemHeight; current <= viewHeight; current = current + itemHeight) {
            drawVerticalLine(canvas, mBitmapBlue, 0, viewHeight);
            drawVerticalLine(canvas, mBitmapPurple, itemWidth - cover, viewHeight);
            drawVerticalLine(canvas, mBitmapRed, itemWidth * 2 - cover, viewHeight);
            drawVerticalLine(canvas, mBitmapPurple, itemWidth * 3 - cover, viewHeight);
            drawVerticalLine(canvas, mBitmapBlue, itemWidth * 4 - cover, viewHeight);
        //    handler.sendEmptyMessageDelayed(0, 100);
      //  }
    }*/

    private void startDrawIdle(Canvas canvas) {
//        Log.i(TAG, "draw idle");
        drawVerticalLine(canvas, mBitmapBlue, 0, itemHeight * 3);
        drawVerticalLine(canvas, mBitmapPurple, itemWidth - cover, itemHeight * 3);
        drawVerticalLine(canvas, mBitmapRed, (itemWidth - cover) * 2, itemHeight * 3);
        drawVerticalLine(canvas, mBitmapPurple, (itemWidth - cover) * 3, itemHeight * 3);
        drawVerticalLine(canvas, mBitmapBlue, (itemWidth - cover) * 4, itemHeight * 3);
    }

    public void checkChildView(Context context) {
        //先清空views里边可能存在的view防止重复
        removeAllViews();
        if (mCarouselViews != null) {
            mCarouselViews.clear();
            for (int res : mImageRes) {
                addImageView(context, res);
            }
            viewCount = getChildCount();
            for (int i = 0; i < viewCount; i++) {
                final View view = getChildAt(i); //获取指定的子view
                mCarouselViews.add(view);
            }
        }
    }

    private void addImageView(Context context, int res) {
        ImageView image = new ImageView(context);
        image.setImageResource(res);
        addView(image);
    }

    /**
     * 初始化 计算平均角度后各个子view的位置
     */
    public void refreshLayout() {
        if (mCarouselViews != null) {
            //    Log.i(TAG, " size = " + mCarouselViews.size());
            for (int i = 0; i < mCarouselViews.size(); i++) {
                double radians = mAngle + 180 - i * 360 / viewCount;
                float x0 = (float) Math.sin(Math.toRadians(radians)) * mCarouselR;
                //     Log.i(TAG, " size = " + radians + "-- " + mCarouselR);
                mCarouselViews.get(i).setTranslationX(x0);
            }
            postInvalidate();
        }
    }

    private void startScaleX() {
        //private float[] multiplesPre = new float[]{1f, 10f, 5f, 10f, 8f, 6f, 15f, 17f, 2f};
        float[] multiples = mThemeStyle ? new float[]{1f, 3f, 4f, 2f, 4f, 5f, 1f, 2f, 4f}
                : new float[]{1f, 10f, 15f, 25f, 18f, 20f, 15f, 17f, 19f};
        long[] times = mThemeStyle ? new long[]{800, 500, 300, 1000, 700, 400, 600, 900, 550}
                : new long[]{2000, 500, 1200, 1000, 1500, 1200, 1500, 1000, 1200};

        if (animatorList != null) {
            animatorList.clear();
        } else {
            animatorList = new ArrayList<>();
        }
        if (mCarouselViews != null) {
            for (int i = 0; i < mCarouselViews.size(); i++) {
                ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(mCarouselViews.get(i), "scaleX", 1f, multiples[i], 1f);
                objectAnimator.setRepeatMode(ValueAnimator.RESTART);
                objectAnimator.setRepeatCount(ValueAnimator.INFINITE);
                objectAnimator.setDuration(times[i]);
                objectAnimator.start();
                animatorList.add(objectAnimator);
            }
        }
    }

    private void stopScaleX() {
        Log.i(TAG, "stop scale");
        if (animatorList != null) {
            for (ObjectAnimator item : animatorList) {
                if (item != null && item.isRunning()) {
                    if (item.getTarget() != null) {
                        ((View) item.getTarget()).setScaleX(1f);
                    }
                }
            }
        }
    }

    private void startScaleY() {
        Log.i(TAG, "stop scale");
        if (animatorList != null) {
            for (ObjectAnimator item : animatorList) {
                if (item != null && item.isRunning()) {
                    if (item.getTarget() != null) {
                        item.cancel();
                        item = ObjectAnimator.ofFloat(((View) item.getTarget()), "scaleY", 52f, 0f);
                        item.setDuration(200);
                        item.start();
                    }
                }
            }
            setWillNotDraw(false);
            removeAllViews();
            if (handler != null) {
                handler.removeCallbacksAndMessages(null);
            }
        }
    }

    public void stopAnimation() {
        if (restAnimator != null && restAnimator.isRunning()) {
            restAnimator.cancel();
            startShrink();
        } else {
            setWillNotDraw(false);
            removeAllViews();
            if (handler != null) {
                handler.removeCallbacksAndMessages(null);
            }
        }
    }

    /**
     * 扩散动画
     */
    private void startSpread() {
        if (mAnimationR != null && mAnimationR.isRunning()) {
            mAnimationR.cancel();
        }
        mCarouselR = mOriginalR;
        Log.i(TAG, "start " + mCarouselR);
        mAnimationR = ValueAnimator.ofFloat(1f, mCarouselR);
        mAnimationR.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                mCarouselR = (Float) valueAnimator.getAnimatedValue();
                refreshLayout();
            }
        });
        mAnimationR.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                Log.i(TAG, " spread start");
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                Log.i(TAG, " spread end");
                startAnimRotation(360);
                startScaleX();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        mAnimationR.setInterpolator(new LinearInterpolator());
        mAnimationR.setDuration(300);
        mAnimationR.start();
    }

    /**
     * 收缩动画
     */
    private void startShrink() {
        stopScaleX();
        mAnimationR = ValueAnimator.ofFloat(mCarouselR, 64f);
        mAnimationR.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                mCarouselR = (Float) valueAnimator.getAnimatedValue();
                refreshLayout();
            }
        });
        mAnimationR.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                Log.i(TAG, " shrink start");
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                Log.i(TAG, " shrink end");
                startScaleY();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        mAnimationR.setInterpolator(new LinearInterpolator());
        mAnimationR.setDuration(300);
        mAnimationR.start();
    }

    /**
     * 旋转动画
     *
     * @param resultAngle 旋转角度
     */
    private void startAnimRotation(float resultAngle) {
        Log.i(TAG, " start rotate " + resultAngle + " -- " + mAngle);
        if (restAnimator != null && restAnimator.isRunning()) {
            Log.i(TAG, " animator is running ********");
            restAnimator.cancel();
        }
        /*if (mAngle == resultAngle) {
            return;
        }*/
        Log.i(TAG, " start rotate");
        restAnimator = ValueAnimator.ofFloat(mAngle, resultAngle);
        //设置旋转匀速插值器
        restAnimator.setInterpolator(new LinearInterpolator());
        restAnimator.setRepeatMode(ValueAnimator.RESTART);
        restAnimator.setRepeatCount(ValueAnimator.INFINITE);
        restAnimator.setDuration(3000);
        restAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                //          Log.i(TAG, "startAnimRotation");
                mAngle = (Float) animation.getAnimatedValue();
                refreshLayout();
            }
        });
        restAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
//                Log.i(TAG, " animation 2 start" + mAngle);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
//                Log.i(TAG, " animation 2 end " + mAngle);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
//                Log.i(TAG, " animation 2 cancel");
                mAngle = 0;
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
//                Log.i(TAG, " animation repeat");
            }
        });
        restAnimator.start();
    }

    public void toIdle() {
        mStatus = STATE_IDLE;
        stopAnimation();
    }

    public void toSpeak() {
        mStatus = STATE_LISTEN;
        stopAnimation();
    }

    public void toListen() {
        mStatus = STATE_SPEAK;
        setWillNotDraw(true);
        checkChildView(mContext);
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
            startSpread();
        }
    }

    public void release() {
        Log.i(TAG, " release");
        if (mBitmapRed != null) {
            mBitmapRed.recycle();
            mBitmapRed = null;
        }
        if (mBitmapPurple != null) {
            mBitmapPurple.recycle();
            mBitmapPurple = null;
        }
        if (mBitmapBlue != null) {
            mBitmapBlue.recycle();
            mBitmapBlue = null;
        }
        if (animatorList != null) {
            animatorList = null;
        }
        removeAllViews();
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
            handler = null;
        }
        if (restAnimator != null && restAnimator.isRunning()) {
            restAnimator.cancel();
            restAnimator = null;
        }
        if (mAnimationR != null && mAnimationR.isRunning()) {
            mAnimationR.cancel();
        }
        if (mCarouselViews != null) {
            mCarouselViews.clear();
            mCarouselViews = null;
        }
    }

}
