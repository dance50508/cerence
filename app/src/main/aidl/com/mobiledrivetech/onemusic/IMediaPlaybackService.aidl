// IMediaPlaybackService.aidl
package com.mobiledrivetech.onemusic;

// Declare any non-default types here with import statements

interface IMediaPlaybackService {
    void openList(in List playData, int position, int playType);
    void openListFragment(in List playData, int position, int playType);
    void play(int playType);
    void playForHistory(int playType,int duration);
    void pause(int playType);
    void pauseByUser(int playType);
    void stop();
    void prev(int playType);
    void next(int playType);
    void seek(int pos);
    int duration();
    int position();
    int getOnlineBufferingPercent();
    boolean isPlaying(int playType);
    int getQueuePosition();
    int getQueuePositionWithType(int playType);
    List getPlayingMusicDataList();
    List getPlayingMusicDataListType(int playType);
    String getNowPlayingHash();
    int getNowPlayingCategoryID();
    int getNowPlayingSpecialID();
    String getNowPlayingSpotifyID();
    String getNowCurPlaySpotifyTrackID();
    String getNowPlayingSongName();
    boolean isActivated();
    void setActivated(boolean isActivated);
    void openAlarmManager();
    int getPlayType();
    int getCurPlayItemId();
    void setCurPlayKgCoverPosition(int position);
    int getCurPlayKgCoverPosition();
    boolean isNeverPlayBefore();
    void setCallSource(String packageName);
    String getCallSource();
    void setCallForRadioType(String radioType);
    void openListRecommend(in List playData, int position);
    void setQueuePosition(int position);
    void removeQuePostion(int position);
    void updateLoginState(String isLogin);
    //List getSpotifyAppRemote();
}
